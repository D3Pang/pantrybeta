import starttoker

def macroReplace(ctx):
    x = commentStripper(ctx)
    starttoker.macroReplace(x)
    
    currentLine = 1
    return currentLine

def commentStripper(ctx):
    print("COMMENTSTRIPPER")
    commentless = ctx.sData[ctx.sDataIndex].splitlines()
    for i,x in enumerate(commentless):
        s = x.lstrip()
        if s.find(ctx.macroPrep) == 0:
            commentless[i] = x[:x.find(ctx.macroPrep)] + x[len(ctx.macroPrep)+x.find(ctx.macroPrep):]
        
    commentless = "\n".join(commentless)
    ctx.sData[ctx.sDataIndex] = commentless
    return commentless