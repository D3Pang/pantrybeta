import starttoker
from starttoker import ctx as ctx

@starttoker.macro
def genericpantryreplacementfunctionname0(ctx=ctx):
    previousVar = ctx.prev()
    nextVar = ctx.next()
    equals = False
    if nextVar == '=':
        nextVar = ctx.next() #If it is equal the next will be a value
        print("THIS IS ADDED TO NEW STRING: {}".format(nextVar[:2]))
        equals=True#return "{} = {}".format(previousVar, nextVar) # x = 'Done'
    tmpVar = ctx.clean('tmp')
    setup = """if {0} == None:
    {2} = {1}
else:
    {2} = {0}    
""".format(previousVar, nextVar, tmpVar)
    ctx.setupCode(setup)
    
    if equals: tmpVar = '{0} = {1}'.format(previousVar, tmpVar)
    return tmpVar

#tmp = 9
x = None
if x ==None :
    tmp =2 
else :
    tmp =x 
print (tmp )# becomes 2
x =5 
if x ==None :
    tmp__0 =2 
else :
    tmp__0 =x 
print (tmp__0 )#becomes x which equals 5
x =None 
if x ==None :
    tmp__1 ='Done'
else :
    tmp__1 =x 
x =tmp__1 #becomes 'x = Done'
print (x )

x =y =z =d =None 

if x ==None :
    tmp__2 =y 
else :
    tmp__2 =x 
if tmp__2 ==None :
    tmp__3 =3 
else :
    tmp__3 =tmp__2 
print (tmp__3 )
if x ==None :
    tmp__4 =y 
else :
    tmp__4 =x 
if tmp__4 ==None :
    tmp__5 =z 
else :
    tmp__5 =tmp__4 
if tmp__5 ==None :
    tmp__6 =3 
else :
    tmp__6 =tmp__5 
print (tmp__6 )
if x ==None :
    tmp__7 =y 
else :
    tmp__7 =x 
if tmp__7 ==None :
    tmp__8 =z 
else :
    tmp__8 =tmp__7 
if tmp__8 ==None :
    tmp__9 =d 
else :
    tmp__9 =tmp__8 
if tmp__9 ==None :
    tmp__10 =5 
else :
    tmp__10 =tmp__9 
print (tmp__10 *5 )
