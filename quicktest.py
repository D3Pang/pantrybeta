import preparser, pantryContext

ctx = pantryContext.Context()
preStep = preparser.PreParse()

#preStep.preToker('enclosetest.py', ctx)
#preStep.preToker('fstringtest2.py', ctx)
#preStep.preToker('dowhiletest2.py', ctx)#
#preStep.preToker('swaptest.py', ctx)
preStep.preToker('coalescemacrotest2.py', ctx)#Requires no commentStripper in starttoker
#preStep.preToker('backslashMacro.py', ctx)#Requires commentStripper in starttoker 
#preStep.preToker('incrementmacro.py', ctx) 
