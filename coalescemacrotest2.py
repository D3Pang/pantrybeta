import starttoker
from starttoker import ctx as ctx

@starttoker.macro
def ??(ctx=ctx):
    previousVar = ctx.prev()
    nextVar = ctx.next()
    equals = False
    if nextVar == '=':
        nextVar = ctx.next() #If it is equal the next will be a value
        print("THIS IS ADDED TO NEW STRING: {}".format(nextVar[:2]))
        equals=True#return "{} = {}".format(previousVar, nextVar) # x = 'Done'
    tmpVar = ctx.clean('tmp')
    setup = """if {0} == None:
    {2} = {1}
else:
    {2} = {0}    
""".format(previousVar, nextVar, tmpVar)
    ctx.setupCode(setup)
    
    if equals: tmpVar = '{0} = {1}'.format(previousVar, tmpVar)
    return tmpVar

#tmp = 9
x = None
print(x ?? 2) # becomes 2
x = 5
print(x ?? 2) #becomes x which equals 5
x = None
x ??= 'Done' #becomes 'x = Done'
print(x)

x = y = z = d = None

print(x ?? y ?? 3)
print(x ?? y ?? z ?? 3)
print(x ?? y ?? z ?? d ?? 5 * 5)