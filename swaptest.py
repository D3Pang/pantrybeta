import starttoker, os
from starttoker import ctx as ctx
#TEST3
@starttoker.macro
def randVar(ctx=ctx):
    return "'tmp'"

@starttoker.macro
def swap(ctx=ctx):
    x = ctx.next()
    y = ctx.next()
    print("SWAP ACTIVATED")
    #aVar = randVar
    aVar = ctx.clean('tmp')
    return "{2} = {0}; {0} = {1}; {1} = {2}".format(x,y,aVar)

if True:
    if True:
        if False:
            print("Error")
        elif False:
            print("Error")
        else:
            print("Expected")

tmp = 0
tmp__0 = 0
x = 2
y = 3
print(x, y)
swap x y
print(x, y)
x = 2; y = 3; swap x y; z = 3
a = 'end'
print(x, y)