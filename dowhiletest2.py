import starttoker, os
from starttoker import ctx as ctx
#TEST3
@starttoker.macro
def dowhile(ctx=ctx):
    colon = ctx.nextLine()#Eat a colon
    doblock = ctx.nextBlock()
    eatWhile = ctx.next()
    condition= ctx.nextLine() #get the argument of the while loop This includes the colon 
    loopBody = ctx.nextBlock() #Get the while block

    return """{0}
while True:
{2}
{0}
    if not {1}
        break""".format(doblock, condition, loopBody)
    #I can also do"""{doblock} / while not {whileCondition} / {whileLoop}""" if I want to separate the block from the condition

#Start of do..while command formatted according to PEP 315
x = 0
dowhile:
    x += 1
while(x <= 5):
    print("{}".format(x))