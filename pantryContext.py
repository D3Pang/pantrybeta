import aider, aider2, keyword
logging = aider.lumberjack('pantryContext.log', 'pC')
class CtxSection(str):
    'For when user request a section of context in next'

    def __init__(self, contents, ctxObj):
        self.contents = contents
        self.ctx = ctxObj

    def split(self, a):
        'When the string is split into an array take what it is split by and put it in context for later retreval'
        self.ctx.trackOldVar = self.contents.split(a)
        return self.ctx.trackOldVar

class Context:
    'after macro find arguments for it'

    
    def __init__(self, macroPrep='#@!'):
        self.itemize = []
        self.currentTokenized = []
        self.currentLine = -1
        self.sDataIndex = 0 #The file block index
        self.sData= []
        self.previousLine = [0 , None, None]#Tuple containing index location, value of indent, and row start location
        self.previousLineCounter = 0
        self.macroPrep = macroPrep
        self.identifymacroDecorator = '@starttoker.macro'
        self.preparseEndString = None
        self.preparseMacroRenamed = {}
        #NOTE:Adding old var tracking
        self.hygieneNeeded = 0
        self.trackOldVar = [] #list of old variables
        self.trackNewVar = {}
        self.existingVars = []
        self.mtriggers = {}
        #NOTE: END old var tracking
        #NOTE: Enclose housekeeping
        self.pythonPairings = [('{', '}'), ('"', '"'), ('(', ')'), ('[', ']'), ("'", "'")] #Tuples of default parings in Python.
        #NOTE: END Enclose housekeeping



    
    def enclose2(self, encloseList):
        'Takes a list of tuples to find the starting and closing characters that will be part of the macro'
        assert isinstance(encloseList, list), "Enclose takes a list as an argument"
        for x in encloseList:
            assert len(x) == 2, "Tuples must be of length 2"
            assert isinstance(x, tuple), "Enclose only accepts a list of tuples"
            for y in x:
                assert isinstance(y, str), "Tuples must be strings"
        
        #Find y (first iteration is opening second is closing)
        openingNotFound = True
        listCounter = 0
        while openingNotFound:
            a = encloseList[listCounter][0] #The tuple element of the list element
            #Find the next character after the macro have it equal to 'a' 
            macroOpening = self.currentTokenized[self.currentLine][1]#Need to remind myself what the currentTokenized is made up of
            if macroOpening == a:
                openingNotFound = True
            else:
                listCounter += 1
                if listCounter >= len(encloseList):
                    raise Exception("Error: Macro enclosement not found. Rewrite Macro correctly")
        
        del self.currentTokenized[self.currentLine]#Remove from the list what was read out
        
        

    def clean(self, var):
        'Makes a variable hygienic'
        logging.debug("Clean Method started. List of trackOldVar and existingVars {}::::{}".format(self.trackOldVar, self.existingVars))
        i = 0
        while var in self.existingVars or var in self.trackOldVar:
            logging.debug("Clean() found variable unhygienic.")
            if i > 0:
                var = var[:var.rfind('__')] #Finds the __ from the right side which will be correct location
            var = var + "__{}".format(i)
            i += 1
        logging.debug("Clean() returning {}".format(var))
        self.trackOldVar.append(var)
        return var


    def keep(self, var):
        'Add a variable to the list that say not to hygienize.'
        logging.debug("Keep Method started. List of trackOldVar {}".format(self.trackOldVar))
        for x in aider.getVars(var):
            if x not in self.trackOldVar:
                self.trackOldVar.append(x)
                logging.info("This is keep() variable not added already in list: {}".format(x))
            logging.info("This is keep() variable added: {}".format(x))
        logging.debug("Keep Method finished. New List of trackOldVar {}".format(self.trackOldVar))

    def dirty(self,var):
        'Remove a variable from list of variables. This makes the variable removed hygienized'
        logging.debug("Dirty Method started. List of trackOldVar {}".format(self.trackOldVar))
        for x in aider.getVars(var):
            self.trackOldVar = [var for var in self.trackOldVar if var != x]
            logging.info("This is dirty() variable asked to be removed: {}".format(x))
        logging.debug("Dirty Method finished. New List of trackOldVar {}".format(self.trackOldVar))

    def prev(self, *args, **kwargs):
        if self.currentLine - 1 < 0:
            raise Exception('Previous does not support hopping blocks or lines.')
        backStep = 1
        while self.currentTokenized[self.currentLine - backStep] == (1, '', '', '', ''): #Ignores the placeholders to get accurate prev 
            backStep += 1
        before = self.currentTokenized[self.currentLine - backStep][0]
        logging.debug("prev() currentTokenized location {}".format(self.currentTokenized))
        if before == 1 or before == 2:
            value = self.currentTokenized[self.currentLine - backStep][1]
            
        else:
            value = False
        logging.debug("prev() consumed: {}".format(value))

        if kwargs:
            if 'confirm' in kwargs:
                if not self.confirmChecker(kwargs['confirm'], value):
                    value = False
            else:
                raise Exception('Command :{}: not recognized for next()'.format(kwargs))
        if value:
            logging.debug("prev() currentTokenized: {}".format(self.currentTokenized))
            del self.currentTokenized[self.currentLine - backStep]
            self.trackOldVar.append(value)
            self.currentTokenized.insert(self.currentLine-1, (1, '', '', '', ''))
            logging.debug("prev() currentTokenized: {}".format(self.currentTokenized))
        return value

    def next(self, *args, **kwargs):
        self.blockAdvancer()
        self.hygieneNeeded = 1

        if len(args) == 0:
            'No arguments so variables are not contained or separated'
            logging.debug("Next(). currenttokenized {}".format(self.currentTokenized))
            
            value = self.currentTokenized[self.currentLine][1]
            vType = self.currentTokenized[self.currentLine][0]
            logging.debug("Next method. removing currenttokenized currentline: {} {}".format(self.currentLine, self.currentTokenized[self.currentLine]))
            
        else:
            'Otherwise arguments are enclosed'
            value = []
            encloseCount=0
            if args[0] == '(' and len(args) == 1:
                g = ['(', ')']
                pass
            else:
                'If more than one argument other than expected then second argument is enclosing symbol'
                g = [args[0], args[1]]
                
            if self.currentTokenized[self.currentLine][1] ==  g[0]:
                del self.currentTokenized[self.currentLine]
                while self.currentTokenized[self.currentLine][1] != g[1] and encloseCount == 0:
                    print(encloseCount)
                    value.append(self.currentTokenized[self.currentLine][1])
                    del self.currentTokenized[self.currentLine] #remove the element from tokenized array and place in another array
                    if self.currentTokenized[self.currentLine][1] == g[0]:
                        encloseCount += 1
                     
                    elif self.currentTokenized[self.currentLine][1] == g[1]:
                        encloseCount -= 1
                        
                    
                
                del self.currentTokenized[self.currentLine]  #remove enclosing parentheses
                print('VALUEOFINNEXT{}'.format(value))
                stringvalue = ''
                for ele in value:
                    stringvalue += ele
                print(stringvalue)
                
                stringvalue = CtxSection(stringvalue, self)
                value = stringvalue
                
            else:
                raise Exception("There is error in writing a macro. Check syntax") 
        
        if kwargs:
            if 'confirm' in kwargs:
                if not self.confirmChecker(kwargs['confirm'], value):
                    value = False

            #XXX:Enclose Argument
            elif 'enclose' in kwargs:
                value = self.enclose(kwargs['enclose'], value)
            #XXX:END Enclose Argument
            else:
                raise Exception('Command :{}: not recognized for next()'.format(kwargs))
            if value:
                del self.currentTokenized[self.currentLine]#Remove from the list what was read out
                self.trackOldVar.append(value)#NOTE: Tracking the old vars in list
                logging.debug("next() consumed {}".format(value))
        else:
            del self.currentTokenized[self.currentLine]#Remove from the list what was read out
            if vType == 1: self.trackOldVar.append(value)#NOTE: Tracking the old vars in list
            logging.debug("next() consumed {}".format(value))

        return value

    def enclose(self, enClo, nextVal):
        assert isinstance(enClo, tuple), "Enclose only accepts a 2-tuple"
        assert nextVal == enClo[0], "Unexpected next value does not match Enclosement parameters"
        newVal = self.next()
        nextVal = newVal
        multiple = 1
        while multiple > 0:
            newVal = self.next()
            logging.debug("Enclose eating {}".format(newVal))
            nextVal += newVal
            if newVal == enClo[0]:
                logging.debug("Enclose multiple added {}".format(multiple))
                multiple += 1
            if newVal == enClo[1]:
                multiple -= 1
                logging.debug("Enclose multiple removed: {}".format(multiple))
        logging.debug("Enclose returns {}".format(nextVal))
        return nextVal


    def confirmChecker(self, typeCheck, value):
        typeCheckVar = typeCheck.lower()
        pythonKeywords = keyword.kwlist
        if typeCheckVar == 'var' or typeCheckVar == 'variable':
            if value not in pythonKeywords:
                return True
            else: #If it is a keyword
                return False
        elif typeCheck in pythonKeywords:
            if typeCheck == value:
                return True
            else:
                return False
        else:
            if typeCheck == value: 
                return True
            else:
                return False
            

    def nextBlock(self, *args):
        logging.info("nextBlock() method started. Consuming rest of line.")
        lineValue = ''
        self.blockAdvancer()
        holdPrevious = False

        try:
            trackIndents = self.currentTokenized[self.currentLine][2][1]#The 4 in (1, 4)
        except IndexError:
            try:
                self.sDataIndex += 1
                holdPrevious = self.currentLine
                self.currentLine = aider2.macroReplace(self)
                self.currentTokenized = cleanPlaceholders(self.currentTokenized)
                
                trackIndents = self.currentTokenized[self.currentLine][2][1]
            except IndexError:
                self.sDataIndex -=1
                self.currentLine = holdPrevious
                logging.debug("Reached end of file no more information to consume")
                raise Exception("IndexError: end of file pantryContext context 'next' consumption is incorrect.")
        
        logging.debug("self.currentLine: {}".format(self.currentLine))
        logging.info("nextBlock() consumed rest of line moving to anticipated block: {}\n{}".format(self.currentTokenized,self.currentTokenized[self.currentLine]))
    
        assert self.currentTokenized[self.currentLine][0] != 0, "Block was not found macro error" #The tokenizer module ends sections with this special token
        #5 is the number indicating 'INDENT' in lexer
        columnPosition = self.currentTokenized[self.currentLine][2][1]
        trackIndents = 0
        while self.currentTokenized[self.currentLine][2][1] >= columnPosition:
            
            if self.currentTokenized[self.currentLine][0] == 0:#If next is endmarkker
                break
            else:
                logging.debug('trackIndents from previous:{}'.format(trackIndents))
                spacing = '    ' * trackIndents
                nextLine = self.nextBlockWhileHelper()
                logging.debug("nextline return tuple:{}".format(nextLine))
                lineValue += spacing + nextLine[0]
                trackIndents += nextLine[1]
            
        logging.debug("nextBlock() finished return value: \n{}".format(repr(lineValue)))
        if holdPrevious: self.currentLine = holdPrevious
        if lineValue.endswith('\n'): 
            if lineValue == '\n':
                pass
            else:
                lineValue = lineValue[:-1]
        return lineValue
        

    def nextBlockWhileHelper2(self):
        logging.info("nextBlockHelper's while loop looking at: {}".format(self.currentTokenized[self.currentLine]))
        
        if self.currentTokenized[self.currentLine][0] != 5: 
            value = self.currentTokenized[self.currentLine][1]
        else:
            value = ''
        
        logging.info("nextBlockHelper deleting: {}".format(self.currentTokenized[self.currentLine]))
        del self.currentTokenized[self.currentLine]
        return value

    def nextBlockWhileHelper(self):
        logging.info("nextBlockHelper's started")
        
        value = self.nextLine('inhouse')
        logging.debug("nextBlockWhileHelper using nextLine(): {}".format(value))
        if value[1]:
                logging.debug("nextblockwhilehelper encountered nextLine end of block{}".format(value))
                logging.debug("readding:{}:{}".format(self.currentTokenized, self.currentLine))
                #self.currentLine -=1    
                self.currentTokenized.insert(self.currentLine,value[1])
                logging.debug("readded:{}:{}".format(self.currentTokenized, self.currentLine))

        rvalue = value[0]
        if value[3]: rvalue += '\n'

        return (rvalue,value[2])


    #stoppingPoint is a tuple cointaining a string value and error message #stoppingPoint=()
        #String value determines where in line to stop and a potential
        # Error message is optional. If not there no error msg will be outputted if stoppingPoint is not found. (This is to allow consumption of entire line.) 
    def nextLine(self, *args, **kwargs):
        'Consumes the entire line unless specified stoppingPoint declared'
        logging.info("nextLine() method started")
        logging.info("nextLine() {}".format(self.currentTokenized))
        logging.info("nextLine() looking at: {}".format(self.currentTokenized[self.currentLine]))
        self.hygieneNeeded = 0
        self.blockAdvancer()
        lineValue = ''
        logging.info("nextLine() looking at: {}".format(self.currentTokenized[self.currentLine]))
        
        if self.currentTokenized[self.currentLine][0] == 59: #The encoding token
            self.currentLine += 1

        rowPosition = self.currentTokenized[self.currentLine][2][0] 
        
        trackIndents = 0
        while self.currentTokenized[self.currentLine][2][0] == rowPosition:
            logging.info("nextLine's while loop looking at: {}".format(self.currentTokenized[self.currentLine]))
            
            value = self.currentTokenized[self.currentLine]
            logging.info("nextLine deleting: {}".format(self.currentTokenized[self.currentLine]))
            del self.currentTokenized[self.currentLine]
            
            if value[0] == 5: trackIndents += 1#if there is an indent track it
            elif value[0] == 6: trackIndents -= 1#if there is a dedent track it
            lineValue += value[1]
            try:
                logging.debug("nextLine() look at currentLine[2][0]{}".format(self.currentTokenized[self.currentLine][2][0]))
                while self.currentTokenized[self.currentLine][2] == '' or self.currentTokenized[self.currentLine][2][0] == 0:
                    self.currentLine += 1
                self.currentTokenized[self.currentLine][2][0]
            except IndexError:
                logging.debug("nextLine() Reached end of block. Encountered IndexError in nextLine() returns: {}".format(lineValue)) #Tends to be endmarker
                if lineValue.endswith('\n'): lineValue = lineValue[:-1]; enterKey = True
                else: enterKey = False
                if args:
                    if 'inhouse' in args:
                        return (lineValue, value, trackIndents, enterKey)
                    else:
                        raise Exception('nextLine() *args not recognized')
                    return lineValue
        if lineValue.endswith('\n'): lineValue = lineValue[:-1]; enterKey = True#Added for user convenience and intuition
        else: enterKey = False
        logging.info("nextLine() next currentline is {}".format(self.currentTokenized[self.currentLine]))
        if args:
            if 'inhouse' in args:
                return (lineValue, None, trackIndents, enterKey)
        return lineValue
   

    #Method to check it it has reached the end of a file block and makes it advance to the next block
    def blockAdvancer(self):
        'If it is at an endmarker then advance to the next file block'
        if self.currentTokenized[self.currentLine][0] == 0: #Tokenizer signal for endMarker
            try:
                self.sDataIndex += 1
                holdPrevious = self.currentLine
                logging.debug("blockAdvancer() sDatablock:{}".format(self.sData[self.sDataIndex]))
                self.currentLine = aider2.macroReplace(self)
                self.currentTokenized = cleanPlaceholders(self.currentTokenized)
                logging.debug("blockAdvancer() tokenize list:{}".format(self.currentTokenized))
                logging.debug("blockAdvancer() block advanced")
            except IndexError:
                self.sDataIndex -= 1
                self.currentLine = holdPrevious
                logging.debug("blockAdvancer: Reached end of file no more information to consume")
                raise Exception("IndexError: end of file 'next' consumption is incorrect.")
        else:
            logging.debug("blockAdvancer()- no endmarker detected did not advance block")

    #Method to add line above where the macro is located. This line is the same indentation level as the line the macro is located in.
    def setupCode2(self, codeStr):
        setupList = list(aider.toker(codeStr)) #Tokens to be added
        setupList = setupList[1:-1] #To get rid of tokenize encoding and endmarker
        logging.debug("setupCode()'s tokenize list: {}".format(setupList))
        rowAdjustment = setupList[-1][2][0] - 1
        #indent = (5, self.previousLine[1], '', '', '')
        #dedent = (6, '', '', '', '')
        if self.previousLine[1]:
            setupList.insert(0, (5, self.previousLine[1], '', '', ''))
            setupList.append((4, '\n', '', '', '')) #Adds newline so that user does not have to worry about it
            setupList.append((6, '', '', '', ''))
        
        logging.debug("setupCode adjusting: {}\n{}".format(self.currentTokenized[self.previousLine[0]], self.previousLine))
        if self.currentTokenized[self.previousLine[0]][0] == 6:
            pass
        self.previousLineCounter += 1
        for x in reversed(setupList): #Reversed so when inserted into the list it is inserted in the right order.
            self.currentTokenized.insert(self.previousLine[0]+self.previousLineCounter, x)
        self.currentLine += len(setupList) - 1
        self.previousLine[0] += len(setupList) - 1
        self.previousLine[2] += rowAdjustment
        logging.debug("setupCode's new previousLine:{}::{}\n{}".format(rowAdjustment,self.previousLine, self.currentTokenized))
        
    def setupCode(self, codeStr):

        logging.debug("setupCode() current tokenized {}\n{}".format(self.currentTokenized[self.currentLine],self.currentTokenized))
        currentRow = self.currentTokenized[self.currentLine][2][0]#Current row position of the macro called
        insertHere = None
        for counter,(tType, tName, tStart, _, _) in enumerate(self.currentTokenized):
            try:
                if currentRow == tStart[0]:#same row as where macro found?
                    if tType == 6: insertHere = counter + 1
                    else: insertHere = counter - 1 #To insert at the area before
                    try:
                        if self.currentTokenized[insertHere][2][0] < currentRow:
                            insertHere += 1
                    except IndexError:
                        pass
                    logging.debug("setupCode found where to insert {}:::{}".format(insertHere, self.currentTokenized[insertHere]))
                    break
            except IndexError:
                pass


        setupList = list(aider.toker(codeStr)) #Tokens to be added
        setupList = setupList[1:-1] #To get rid of tokenize encoding and endmarker

        nsetupList = []
        for tName,tType, _, _, _ in setupList:
          nsetupList.append((tName, tType, '', '', ''))
        setupList = nsetupList


        logging.debug("setupCode()'s tokenize list: {}".format(setupList))
        if self.previousLine[1]:
            setupList.insert(0, (5, self.previousLine[1], '', '', ''))
            setupList.append((4, '\n', '', '', '')) #Adds newline so that user does not have to worry about it
            setupList.append((6, '', '', '', ''))
        
        logging.debug("setupCode adjusting: {}\n{}".format(self.currentTokenized[self.previousLine[0]], self.previousLine))
        if self.currentTokenized[self.previousLine[0]][0] == 6:
            pass
        self.previousLineCounter += 1
        for x in reversed(setupList): #Reversed so when inserted into the list it is inserted in the right order.
            self.currentTokenized.insert(insertHere, x)
        self.currentLine += len(setupList) #- 1
        self.previousLine[0] += len(setupList) - 1
        
        

        logging.debug("setupCode() current tokenized2 {}\n{}".format(self.currentTokenized[self.currentLine],self.currentTokenized))



def start():
    return Context()



def cleanPlaceholders(result):
    logging.debug("cleanPlaceholders() - cleaning {}".format(result))
    cleanResult = []
    for i, (tType, tName, tStart, tEnd, tLine) in enumerate(result):
        if tType == 1 and tName == '':
            pass
        else:
            cleanResult.append((tType,tName,tStart,tEnd,tLine))
    return cleanResult