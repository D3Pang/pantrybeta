import  tokenize, re, ast, starttoker, pantryContext, aider #Reason for starttoker is the iteration of unfinished macros
from io import BytesIO

logging = aider.lumberjack('starttoker.log', 'toker')

ctx = None #Should be initialized by method StartToker where it will have the correct values
replacementName = "genericpantryreplacementfunctionname"

def strToBlock(line):
    'Takes an array that is generated from a file and makes it into an array by blocks'
    nArray = []
    lookForClosingComment = 0
    lookforDefBlock = 0
    for x in line:
        print(repr(x))
        if len(x) - len(x.lstrip()) > 0: #There is a whitespace/indent at string
            try:
                nArray[-1] += x
            except:
                print("ERROR: File has indent on first line")
                raise
        elif ctx.identifymacroDecorator in x:
            lookforDefBlock = 1
            nArray.append(x)
        elif lookforDefBlock == 1:
            try:
                lookforDefBlock = 0
                nArray[-1] += x
            except:
                print("ERROR: Array out of bounds")
                raise
        else:
            if lookforDefBlock == 1:
                lookforDefBlock = 0
            nArray.append(x)
    
    return nArray

def filetoBlock(arr):
    nArr = []
    openPairs = {"(":[0, ")"], "{":[0, "}"], "[":[0, "]"]}
    quotePairs =  {"'''":0, '"""':0}
    count = False
    addtoPrev = False
    triplequote = None
    continueGroup = False

    for line in arr:
        if addtoPrev:
            if len(nArr) > 0:
                nArr[-1] += line
            else:
                print(line)
                nArr.append(line)
        else:
            nArr.append(line)


        if triplequote:
            if triplequote in line:
                line = line.split(triplequote)
                line=''.join(line[1:])
                addtoPrev = False
                triplequote = None
            else:
                pass
        #else:#Indent the rest


        quotetypes = 0
        for x in quotePairs:
            if x in line:
                triplequote = x #Marks the quote that last appears so if it is just one triple quote it will mark that quote
                quotetypes += 1

        if quotetypes == 2:
            #find the first quote that appears
            print(line)
            double = line.find('"""')
            single = line.find("'''")
            assert double >= 0, "Should not be negative"
            assert single >= 0, "Should not be negative"
            if double < single:
                triplequote = '"""'
            else:
                triplequote = "'''"


        elif quotetypes == 1:
            #don't count any other openpairs
            if line.count(triplequote)%2 == 0:
                triplequote = None
                #don't worry about quotes then
            else:
                pass

        else: #No tiple quotes just continue 

            for x in openPairs:

                if x in line:
                    openPairs[x][0] += line.count(x)#Finds opening pairs

                if openPairs[x][1] in line:
                    openPairs[x][0] -= line.count(openPairs[x][1]) #Finds closing pairs in line

            count = False
            for key in openPairs:
                if openPairs[key][0] > 0:
                    print("CHANGED")
                    count = True

            if count:
                addtoPrev = True
            else:
                addtoPrev = False
            print(line)
            if line.endswith("\\\n"):
                print(line)
                addtoPrev = True

    if addtoPrev == True: #For when it ends the file with still something needing to be closed
        print(openPairs)
        raise Exception("Error: something might not be closed properly")
    
    return nArr

def hygienize(s):
    #NOTE:AST Hygiene implementation
    tree = ast.parse(s)
    for node in ast.walk(tree):
        if isinstance(node, ast.Name):
            if node.id not in ctx.trackOldVar: #Add new vars into set so none is repeated
                if node.id not in ctx.trackNewVar:
                    i = 0
                    hygic = node.id
                    while hygic in ctx.existingVars:
                        if i > 0:
                            hygic = hygic[:hygic.rfind('__')]
                        hygic = hygic + "__{}".format(i)
                        i += 1
                    ctx.trackNewVar[node.id] = hygic
                
                node.id = ctx.trackNewVar[node.id]
                
def hygienize2(s):
    for i,(tType, tName, a, b, c) in enumerate(s):
        if tType == tokenize.NAME and tName in ctx.trackNewVar:
            tName = ctx.trackNewVar[tName]
            s[i] = (tType, tName, a, b, c)
    


mtriggers = {}#A dictionary of macro trigger words Everytime a new one is defined it is added to the list with the function attached

def macro(f):
    mtriggerName = f.__name__
    logging.debug("macro Decorator. Macro added {}".format(mtriggerName))
    mtriggers[mtriggerName] = f




def startToker(s, s2, s3, ctxObject, renamedMacros):
    nData = []
    global ctx 
    ctx = pantryContext.Context(ctxObject)
    ctx.preparseMacroRenamed = renamedMacros
    ctx.existingVars = s3
    with open("outputFiletestTemp.py", 'r') as r2File:
        varData = r2File.read()
    with open(s, 'r') as rFile:
        sData = rFile.readlines()
        
        tree = ast.parse(varData)
        nSet = set()
        for x in tree.body:
            if isinstance(x, ast.Assign):
                print(ast.dump(x))
                for x2 in x.targets:
                    try:
	                    nSet.add(x2.id)
                    except AttributeError:
                        try:
	                        for x3 in x2.elts:
	                            nSet.add(x3.id)
                        except AttributeError:
                            pass

        nSet = list(nSet)
        ctx.existingVars = nSet
        logging.debug("VARIABLE LIST\n{}".format(ctx.existingVars))
        with open(s2, 'w+') as wFile:
            logging.debug("startToker method readlines:\n{}".format(sData))
            
            sData = strToBlock(sData)#Makes the blocks of the file into one element instead of several
            logging.debug("starttoker strToBlock result:\n{}".format(sData))
            
            #XXX: START OF COMPOSABLE
            print('STARTING COMPOSABLE BLOCK')
            affectedIndexes = [] 
            unfinishedMacros = [] #Macros that have macros in its definition
            completeMacros = {} #Macros that do not need to be expanded. This will contain the macro name as the key and the function reference of that macro
            '''trackDefMacro = 0'''
            for i,element in enumerate(sData):#First search all elements for the macro definitions
                if ctx.identifymacroDecorator in element:#Is it the definition of a macro?
                    affectedIndexes.append(i) #Adds the index number of the block that is a macro
                    
                    print('THIS IS ELEMENT WITH @STARTTOKER\n{}'.format(element))
                    if ctx.macroPrep in element:#Does the macro definition have macros
                        unfinishedMacros.append(element) #Add the macro to an array to expand it before it can be used to expand
                        print('HERE IS UNFINISHEDMACROS {}'.format(unfinishedMacros))
                    else: #It contains no macros found by preparse
                        macroName = r'def (.*)\(.*\)' 
                        print('HERE IS ELEMENT{}'.format(element))
                        match = re.search(macroName, element)
                        
                        
                        completeMacros[match.group(1)] = mtriggers[match.group(1)]#Add to dictionary the macro name, function pair with completed macros
                        print('HERE IS COMPLETEMACROS{}'.format(completeMacros))

            
            while len(unfinishedMacros) > 0: #While there still are unfinished macros
                unfinishedList = unfinishedMacros[:]
                for element in unfinishedList:
                    newDefMacro = macroReplace(element, completeMacros) #macro replace macro words that are completed if the macro isn't in the completed list it should not remove the preparse macro comment
                    if ctx.macroPrep in newDefMacro: 
                        pass
                    else:#if it isn't in there add it to list of completed macros
                        macroName = r'def (.*)\(.*\)'
                        match = re.search(macroName, newDefMacro)
                        logging.debug('Unfinished macro finishing:\n{}'.format(newDefMacro))
                        exec(newDefMacro)#Evaluate the function and add that function to the dictionary it should rewrite the function in the mtriggers dictionary
                        
                        
                        
                        completeMacros[match.group(1)[:-1]] = mtriggers[match.group(1)[:-1]]
                        unfinishedMacros.remove(element)
                        iNumber = sData.index(element)
                        del sData[iNumber]
                        sData.insert(iNumber, newDefMacro)
                    

            logging.debug("This is sData that does into macroReplace {}".format(sData))
            
            #XXX:END OF COMPOSABLE

            logging.debug('mtriggers as it enters macroreplacement: {}'.format(mtriggers))
            
            ctx.sData = sData
            sData = ctx.sData
            ctx.mtriggers = mtriggers
            while True:
                if ctx.macroPrep in sData[ctx.sDataIndex]:
                    logging.debug("MACROPREP is in this element {}".format(sData[ctx.sDataIndex]))
                    logging.debug("sData{}".format(sData[ctx.sDataIndex]))
                    #commentStripper(ctx)
                    element = macroReplace(sData[ctx.sDataIndex])
                    logging.debug("sData{}".format(sData[ctx.sDataIndex]))
                    logging.debug("MacroReplace returned: {}".format(element))
                else:
                    element = sData[ctx.sDataIndex]
                logging.debug("starttoker() appending to nData: {}".format(element))
                nData.append(element)
                try:
                    ctx.sDataIndex += 1
                    sData[ctx.sDataIndex]
                except IndexError:
                    ctx.sDataIndex -= 1
                    logging.debug("Reached the end of sData array. Exiting")
                    break
                    

            logging.debug("starttoker() nData:\n{}".format(nData))
            nData = nData[:-1]#XXX: Removing end

            sData = ''.join(nData)

            logging.debug("Writing to Outputfile2\n{}".format(sData))
            wFile.write(sData)






def strtoBlockTest():#NOTE:TESTING strToBlock
    s = aider.blockTest()
    x = s.split('\n')
    arr = []
    for y in x:
        y += '\n'
        arr.append(y)
    #arr = ['import moduletest, os\n', 'from moduletest import ctx as ctx\n', '\n', '@moduletest.macro\n', 'def swap():\n', '    x = ctx.next()\n', '    y = ctx.next()\n', '    print("SWAP ACTIVATED")\n', '    return "tmp = {0}; {0} = {1}; {1} = tmp".format(x,y)\n', '\n', "print('hi')\n", "'''swap a b:\n", '    swap a b\n', '    return True\n', "print('test')'''\n", '\n', 'print("END")']
    print(strToBlock(arr))





def makeSure(s, s2, msg=None):
    if not msg:
        msg = "next() call did not get expected {}".format(s2)
    assert s==s2, "{}".format(msg)


def hasMacro(a, macroList = mtriggers):
    for x in macroList:
        if x in a:
            return True
    else:
        return False




def macroReplace(readline, mtriggers = mtriggers):
    logging.info("macroReplace() started")
    result = []
    logicBitIgnore = 0 #To make it ignore the def of the macro
    ctx.currentTokenized = list(tokenize.tokenize(BytesIO(readline.encode('utf-8')).readline))

   
    newList = []
    for (tType, tName, tStart, tEnd, tLine) in ctx.currentTokenized:
        newList.append((tType,tName, tStart, tEnd, ''))#NOTE:OLDVERSION#, tStart, tEnd, ''))
    ctx.currentTokenized = newList
    logging.debug("List of Tokens added {}".format(ctx.currentTokenized))
    

    
    positionColumnAdjust = 0#To adjust the position of tokens
    positionRowAdjust = 0

    blockLeft = True
    ctx.currentLine = 0
    while blockLeft: #While there are still tokens to look at
        #logging.debug("whileLoop currenttokenized:\n{}\ncurrentline: {}".format(len(ctx.currentTokenized), ctx.currentLine))
        (tType, tName, tStart, tEnd, tLine) = ctx.currentTokenized[ctx.currentLine] 
        logging.debug("macroreplace while loop looking at:{}::{}".format(ctx.currentLine,ctx.currentTokenized[ctx.currentLine]))
        #logging.debug("macroReplace() while blockLeft position: {}\n{}".format(ctx.currentLine, ctx.currentTokenized))
        #XXX:Keep previous line stored
        #When the current token has is on a different row then that means it has started on a new line. So the index of the token is stored.
        try:
            if ctx.previousLine[2] != tStart[0]:
                logging.debug("previousLine context added: {}::{}".format(ctx.previousLine, ctx.currentTokenized[ctx.previousLine[0]]))
                ctx.previousLine[0] = ctx.currentLine
                ctx.previousLine[2] = tStart[0]
                #I need to start appending at the end of that line with the same indentation as the next line
                if tType == 5: #If it is an indentation
                    ctx.previousLine[1] = tName
                logging.debug("new previousLine context: {}::{}".format(ctx.previousLine, ctx.currentTokenized[ctx.previousLine[0]]))
        except IndexError:
            print('Encountered a placeholder. Moving on.')
        #XXX:END keep previous line

        if tType == tokenize.NAME and tName == 'def':#If the tuple has it as 'def' then the next tuple is not a replaceable macro
            logicBitIgnore = 1
            
        elif logicBitIgnore == 1: #Ignores the next word for example 'def swap'. swap will be ignored
            logicBitIgnore = 0
            

        elif tType == tokenize.COMMENT and tName[:len(ctx.macroPrep)] == ctx.macroPrep: 
            del ctx.currentTokenized[ctx.currentLine]#Remove the ctx.macroPrep and replace the tokenize array
            properIndent = ' ' * tStart[1]
            officialString = properIndent + tName[len(ctx.macroPrep):]
            tokeString = list(tokenize.tokenize(BytesIO((officialString).encode('utf-8')).readline))
            tokeString = tokeString[1:-1]#So that it does not take encoding and endmarker
            pos = ctx.currentLine
            for token in tokeString:
                ctx.currentTokenized.insert(pos,(token[0],token[1], token[2], token[3], ''))
                pos += 1
            ctx.currentTokenized.insert(ctx.currentLine, (1, '', '', '', ''))#Position adjustment so that it reads again
            
            logging.debug('THISISAFTERCOMMENT{}'.format(ctx.currentTokenized))
            logging.debug('THIS IS POS {}'.format(ctx.currentLine))

        
        elif tType == tokenize.NAME and tName in mtriggers: 
            print("Found {}".format(tName))
            #NOTE: TEST
            print('THIS IS MTRIGGERS DELETED{}'.format(ctx.currentTokenized[ctx.currentLine+1]))
            del ctx.currentTokenized[ctx.currentLine]
            macroString = mtriggers[tName](ctx)#call the function in the dictionary
            logging.debug("macroReplace() mtrigger function returned:\n{}".format(repr(macroString)))
            
            #XXX: Analyze the returned value for macros
            if hasMacro(macroString):
                logging.debug("Going through hasMacro()")
                macroReplace(macroString)
            #XXX: End returned macro macro analysis

            if ctx.hygieneNeeded == 1: hygienize(macroString)#XXX:Changed location take note of any changes
            
            
            tokeString = list(tokenize.tokenize(BytesIO(macroString.encode('utf-8')).readline))
            tokeString = tokeString[1:-1]
            
            logging.debug("tokeString {}".format(tokeString))
       
            pos = ctx.currentLine
            logging.debug("pos {}\n{}".format(pos,ctx.currentTokenized[ctx.currentLine]))
            logging.debug("currentTokenized inserting for loop: {}".format(ctx.currentTokenized))
            for token in tokeString:#NOTE: I do this because the start and end doesn't match up so I get rid of them so they pose no problem. If I want these arguments then I need to add addition to them to get the proper values when I add the macro into the array
                ctx.currentTokenized.insert(pos,(token[0], token[1]))
                pos += 1
            ctx.currentTokenized.insert(ctx.currentLine, (1, '', '','',''))#Position adjustment

            logging.debug("currentTokenized for loop done: {}".format(ctx.currentTokenized))

            
            retoken = ctx.currentTokenized
            
            
            
            newretoken = []
            for token in retoken:
                ttType = token[0]
                ttName = token[1]
                newretoken.append((ttType, ttName))
            retoken = newretoken
            logging.debug("Retoken array: {}".format(retoken))
               
            for index in range(len(retoken)-1, -1, -1):
                tType, tName = retoken[index]
                if tType == 1 and tName == '':
                    del retoken[index]


            logging.debug("Retoken removal of placeholders {}".format(retoken))
            
            retoken = tokenize.untokenize(retoken).decode('utf-8')
            retokeList = list(retoken)
            
            x=0
            while x < len(retokeList) and list(retoken)[x] == ' ' :
                
                x+=1
 
            retoken = retoken[x:]#For some reason it adds a space at untokenize here
            logging.debug("retoken:\n{}".format(retoken))
            retoken = list(tokenize.tokenize(BytesIO(retoken.encode('utf-8')).readline))
            

            if ctx.hygieneNeeded == 1: hygienize2(retoken)
            logging.debug("retoken: {}:{}:{}".format(ctx.currentLine,len(retoken),retoken))
            logging.debug("ReToken Current: {}".format(retoken[ctx.currentLine]))
            for i in range(x):
                retoken.insert(ctx.currentLine-1, (1, '', '', '', ''))
            retoken.insert(ctx.currentLine-1, (1, '', '', '', ''))
            
            ctx.currentTokenized[:] = retoken #NOTE: The error here is that it will still continue to use the old list even though I changed it here. 
            
            ctx.currentLine -= 1
            

        else:
            pass


        # break out of loop recheck when body is finished
        try:
            ctx.currentLine += 1
            ctx.currentTokenized[ctx.currentLine]
        except IndexError:
            'Break out of while blockLeft loop'
            logging.debug("Finished Block exiting macroReplace while loop")
            ctx.currentLine -= 1
            blockLeft = False

    for tType, tName, tStart, tEnd, tLine in ctx.currentTokenized:
        result.append((tType, tName, tStart, tEnd, tLine))

    ctx.previousLine = [0, None, None] #reset the previous line for the next block
    ctx.previousLineCounter = 0
    logging.debug("macroReplace() result {}".format(result))
    block = [indent for indent in result if indent[0] == 5]


    #NOTE:GET RID OF THE PLACEHOLDERS USED FOR ITERATION
    simpResult = []
    for x in result:
        simpResult.append((x[0], x[1]))
    result = simpResult
    logging.debug('HERE IS NEW RESULT {}'.format(result))

    cleanResult = []
    for i, (tType, tName) in enumerate(result):
        if tType == 1 and tName == '':
            pass
        else:
            cleanResult.append((tType,tName))
    result = cleanResult
    #NOTE:RID OF PLACEHOLDERS END


    nresult = tokenize.untokenize(result).decode('utf-8')
    '''for i in range(len(block)):
        nresult = '\t' + nresult'''
    logging.debug("macroReplace() returning {}".format(nresult))
    return nresult


def commentStripper(ctx):
    print("COMMENTSTRIPPER")
    commentless = ctx.sData[ctx.sDataIndex].splitlines()
    logging.debug("commentStripper(): returns {}".format(commentless))
    for i,x in enumerate(commentless):
        s = x.lstrip()
        comment=True
        while comment:
            print(s)
            if s.find(ctx.macroPrep) == 0:
                commentless[i] = x[:x.find(ctx.macroPrep)] + x[len(ctx.macroPrep)+x.find(ctx.macroPrep):]
                x = commentless[i]
                s = commentless[i].lstrip()
            else:
                comment=False
        #x.find(ctx.macroPrep)
        #commentless[i] = x.lstrip()
    commentless = "\n".join(commentless)
    ctx.sData[ctx.sDataIndex] = commentless +'\n'
    logging.debug("commentStripper(): returns {}".format(commentless))
    return commentless