import tokenize, re, ast, os, pantryContext, aider
from io import BytesIO

logging = aider.lumberjack('preparse.log', 'PreParse')


class PreParse:
    '''This class contains some housekeeping things to help ensure that the preparsing of the file
    are free of common errors as much as possible.'''
    def __init__(self):
        self.catchCall = 0 #Catches when a macro function is defined
        self.catchBlock = 0
        self.catchDef = (0, None)
        self.catchMacroCascade = 0#XXX:Added for name errors
        self.macrPreToke = []
        self.replacedMacroNames = {}
        self.replacedPreParseMacroNames = {}
        self.replacementmacroName = 'genericpantryreplacementfunctionname0'
        self.macroPrep = '#@!'
        self.addCompileString = None
        self.identifymacroDecorator = '@starttoker.macro'
        
    def commenter(self, line, temp = True):
        logging.debug("{}\n{}{}".format(line,self.catchDef, self.catchBlock))
        regex = r'\(.*\)' #Regex to search for anything within parentheses
        if line.lstrip().startswith(self.identifymacroDecorator):
            self.catchCall = 1
            return line
        elif self.catchCall == 1:
            if line.lstrip().startswith('def '): #Finds a method by looking for 'def'
                look = line.split(' ')
                found = next(x for x in look if 'def' in look)
                ind = look.index(found) + 1
                mObj = re.search(regex, look[ind]) 
                if mObj == None: #Make it so that if it is none it spits out error
                    raise Exception('CusPreParseError: Cannot find function name with Regex.')
                result = look[ind][:mObj.start()]
                self.macrPreToke.append(result)
                self.catchDef = (1, result)#XXX:Added to prevent looping macro
                logging.debug("Added new macro. List of macroPreToke {}".format(self.macrPreToke))
                self.catchCall = 0
                self.catchBlock = 1

                #XXX:ADDDING syntax error function name avoidance
                tester = "def " + result + "(): \n\tpass"
                try:
                    ast.parse(tester)
                except SyntaxError:
                    line = look[0] + ' ' + self.replacementmacroName + look[ind][mObj.start():]
                    self.replacedMacroNames[self.replacementmacroName] = result
                    self.replacedPreParseMacroNames[result] = self.replacementmacroName
                    logging.debug("Syntax breaking macro name detected. Renaming. {}".format(self.replacedMacroNames))
                    self.replacementmacroName = self.replacementmacroName[:-1] + str(int(self.replacementmacroName[-1]) + 1)
                    #logging.debug(self.replacementmacroName)
                #XXX:END function name avoidance


                return line
            
        elif len(self.macrPreToke) >= 1 and any(x in line for x in self.macrPreToke): 
            self.catchBlock = 1
            self.catchMacroCascade = 1 #XXX: ADDED for name error prevention
            leadWhitespace = len(line) - len(line.lstrip())

            skip = False

            if not line[:leadWhitespace].startswith('#'):
                logging.debug("looking at line with macro: {}".format(line))
                for x in self.replacedPreParseMacroNames:
                    logging.debug("for loop: {}".format(x))

                    if self.catchDef[0] == 1 and self.catchDef[1] == x:#XXX:If statement is a temp fix for iterative looping macros old version is just the else statement
                        logging.debug("Skipping")
                        skip = True
                        
                    else:
                        line = line.replace(x, self.replacedPreParseMacroNames[x])
            
            if skip:
                return line

            logging.debug("ANY macrPreToke Activated: \n{}".format(line))
            return line[:leadWhitespace] + self.macroPrep + line[leadWhitespace:] 
        elif self.catchBlock == 1: 
            leadWhitespace = len(line) - len(line.lstrip())
            if line.lstrip().startswith('return '): 
                return line #NOTE: This is for those methods that if was in a block would have the return avaialable 
                
            elif leadWhitespace >= 1: #If there is a space in front of the line that means that it is a block
                #leadWhitespace = len(line) - len(line.lstrip())
                if self.catchDef[0] == 1:
                    return line
                logging.debug("CATCHBLOCK Activated: \n{}".format(line))
                return line[:leadWhitespace] + self.macroPrep + line[leadWhitespace:]
            else:
                logging.debug("Not block")
                self.catchDef = (0, None)
                self.catchBlock = 0
                return self.commenter(line, temp)
        #XXX:ADDED to comment out everything after macro is found in order to prevent name errors and to account for name errors 
        elif self.catchMacroCascade == 1 and temp:
            logging.debug("CASCADE ACTIVATED \n{}".format(line))
            if line.lstrip().startswith('def '):
                self.catchMacroCascade = 0
                return line
            elif line.lstrip().startswith('return '):
                return line
            else:
                leadWhitespace = len(line) - len(line.lstrip())
                if len(line)== leadWhitespace:
                    logging.debug("NEWLINE SKIP")
                    return line[:leadWhitespace] + line[leadWhitespace:]
                else:
                    logging.debug("CASCADE ADDED macroprep")
                    return line[:leadWhitespace] + self.macroPrep + line[leadWhitespace:]
        #XXX:End Add to comment out everything
        else:
            logging.debug("Nothing else")
            return line

    
    def ensurePrepUnique(self, f):
        'This is to make sure that if the symbol combination is used in the file that macroPrep of Context object is unique'
        while self.macroPrep in f:
            self.macroPrep = self.macroPrep + '%'
    

    def preToker(self, s, ctx):
        newFilename='outputFiletest.py'
        newfilepath = None

        with open(s, 'r') as rFile:
            with open("outputFiletestTemp.py", 'w+') as w2File:
                p2 = PreParse()
                for line in rFile:
                    aArray = p2.commenter(line, False)
                    #print(aArray)
                    w2File.write(aArray)
        with open(s, 'r') as rFile:
            newfilepath = '{}{}'.format(os.path.abspath(rFile.name)[:-(len(os.path.basename(rFile.name)))], newFilename)
            print(newfilepath)
            sfArray = os.path.splitext(newfilepath)
            secondFile = sfArray[0] + '2' + sfArray[1]
            print(secondFile)
            with open(newfilepath, 'w+') as wFile:
                for line in rFile:
                    aArray = self.commenter(line)
                    wFile.write(aArray)
        with open(newfilepath, 'a') as wFile:
            #XXX: CTX object change
            ctx.macroPrep = self.macroPrep
            #XXX:END CTX object change

            
            self.addCompileString="\nstarttoker.startToker('{0}', '{1}', sorted(locals()), '{2}', {3})".format(newfilepath, secondFile, ctx.macroPrep, str(self.replacedMacroNames))
            ctx.preparseEndString = self.addCompileString
            wFile.write(self.addCompileString)
        
        with open(newfilepath) as f:
            print("Running Pantry macros through the file. \n.\n.\n.\n.\n.")
            code = compile(f.read(), 'outputFiletest.py', 'exec')
            exec(code)
            print("Pantry completed!")

    