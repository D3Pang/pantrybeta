import starttoker, os
from starttoker import ctx as ctx

@starttoker.macro
def \(ctx=ctx):
    nextVar = ctx.next()
    continuation = ctx.nextBlock()
    if nextVar == '\n':
        return '''(\n{})'''.format(continuation)
    else:
        return '\{}'.format(nextVar)

val = 5

assert val > 4, "Variable val is not greater than 4"

assert val > 4, (
    "Variable val is not greater than 4")

assert val > 4, \
    "Variable val is not greater than 4"