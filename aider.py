import tokenize, logging
from io import BytesIO

def lumberjack(fileName, name, filemode='w', lFormat='%(asctime)s.%(msecs)d: %(name)s - %(levelname)s - %(message)s', level=logging.DEBUG):
    'Quick set up of logging module '
    handler = logging.FileHandler(fileName, mode=filemode)
    handler.setFormatter(logging.Formatter(lFormat,'%m-%d-%y %H:%M:%S'))

    logger = logging.getLogger(name)
    logger.setLevel(level)
    logger.addHandler(handler)

    return logger



def toker(s):
    tokenObject = tokenize.tokenize(BytesIO(s.encode('utf-8')).readline)
    return tokenObject

def getVars(s):
    aList=[]
    for tType, tName, _, _, _ in toker(s):
        if tType == tokenize.NAME:
            aList.append(tName)
    return aList

def blockTest():
    s = """import moduletest, os
from moduletest import ctx as ctx

@moduletest.macro
def swap():
    x = ctx.next()
    y = ctx.next()
    print("SWAP ACTIVATED")
    return "tmp = {0}; {0} = {1}; {1} = tmp".format(x,y)
    
print('hi')
'''swap a b:
    swap a b
    return True
print('test')'''
    """
    return s


def detectTriple(line, number, nArray):
    if line.startswith('#'):#If the line is a comment then it is a comment 
        if number == 0:
            nArray.append(line)
        else:#If there is already a triple quote then it will close triple quote by adding to block
            number -= 1
            nArray[-1] += line
    else:#If no comment 
        #What happens when the triple quote does not start the line but ends  I think I can just disregard it and put it into a new element
        if number == 0:
            number+= 1
            try:
                nArray[-1] += line
            except:
                nArray.append(line)
        else: #tripleDouble > 0
            try:
                number -= 1
                nArray[-1] += line
            except:
                print("Error: TripleQuote string error")
                raise 
    return (number, nArray)

def strToBlock(oList):
    'Takes an array that is generated from a file and makes it into an array by blocks'
    nArray = []
    lookForClosingComment = 0
    lookforDefBlock = 0
    tripleDouble = 0
    tripleSingle = 0
    for x in oList:
        print(repr(x))
        if len(x) - len(x.lstrip()) > 0: #There is a whitespace/indent at string
            try:
                nArray[-1] += x #Adds line to the block
            except:
                print("ERROR: File has indent on first line")
                raise
        
        elif lookforDefBlock == 1:
            try:
                lookforDefBlock = 0
                nArray[-1] += x
            except:
                print("ERROR: Array out of bounds")
                raise
        #Checks if line starts with # comment if so then disregard
        elif '"""' in x:
            tripleDouble, nArray = detectTriple(x, tripleDouble, nArray)           
        elif "'''" in x:
            tripleSingle, nArray = detectTriple(x, tripleSingle, nArray)
        elif tripleDouble > 0 or tripleSingle > 0:
            try:
                nArray[-1] += x
            except:
                print("Error: Block appending due to triple quote string")
                raise
        else:
            if lookforDefBlock == 1:
                lookforDefBlock = 0
            nArray.append(x)
    
    return nArray