import starttoker
from starttoker import ctx as ctx

@starttoker.macro
def ++(ctx=ctx):
    previousVar = ctx.prev(confirm='var')
    tmpVar = ctx.clean('tmp')
    print("MACRO FOR INCREMENT")
    if previousVar:
        print("PREV FOR INCREMENT")
        setup = """{0} = {1}
{1} += 1
""".format(tmpVar, previousVar)
        ctx.setupCode(setup)
        return tmpVar

    nextVar = ctx.next(confirm='var')
    if nextVar:
        print("NEXT FOR INCREMENT")
        setup = """{0} += 1
{1} = {0}
""".format(nextVar, tmpVar)
        ctx.setupCode(setup)
        return tmpVar
    
    raise Exception("Increment Macro does not match with expected pattern.")

#tmp = 9
x = 0
y = 0
x ++ #tmp = x; x += 1; \n tmp
print(x) 
x = 0
++ x #x += 1; tmp = x;\n tmp
print(x) 
x = 0
y = ++ x + x ++ # x += 1 tmp = x; \n tmp__1 = x; x+=1\n  y = tmp + tmp__1
print(x)
print(y)
x = 0
y = ++ x + ++ x + ++ x + x ++
print(x)
print(y)
