#from preparser import preStep as preStep
import aider, aider2
#logging.basicConfig(level=logging.DEBUG, filename='pantryContext.log', filemode='w', format='%(name)s - %(levelname)s - %(message)s')
logging = aider.lumberjack('pantryContext.log', 'pC')
class CtxSection(str):
    'For when user request a section of context in next'

    def __init__(self, contents, ctxObj):
        self.contents = contents
        self.ctx = ctxObj

    def split(self, a):
        'When the string is split into an array take what it is split by and put it in context for later retreval'
        self.ctx.trackOldVar = self.contents.split(a)
        return self.ctx.trackOldVar

class Context:
    'after macro find arguments for it'

    #TODO: Make it so that to change these vars you need to use a method. This is for security.
    def __init__(self, macroPrep='#@!'):
        self.itemize = []
        self.currentTokenized = []
        self.currentLine = -1
        self.sDataIndex = 0 #The file block index
        self.sData= []
        self.macroPrep = macroPrep
        self.identifymacroDecorator = '@starttoker.macro'
        self.preparseEndString = None
        #NOTE:Adding old var tracking
        self.hygieneNeeded = 0
        self.trackOldVar = [] #list of old variables
        self.trackNewVar = {}
        self.existingVars = []
        self.mtriggers = {}
        #NOTE: END old var tracking
        #NOTE: Enclose housekeeping
        self.pythonPairings = [('{', '}'), ('"', '"'), ('(', ')'), ('[', ']'), ("'", "'")] #Tuples of default parings in Python.
        #NOTE: END Enclose housekeeping


    '''def collect(self):
        'takes the next part of the string as arguments'
        #For rest of line put it in the array.'''

    #TODO: Finish enclose method
    def enclose(self, encloseList):
        'Takes a list of tuples to find the starting and closing characters that will be part of the macro'
        assert isinstance(encloseList, list), "Enclose takes a list as an argument"
        for x in encloseList:
            assert len(x) == 2, "Tuples must be of length 2"
            assert isinstance(x, tuple), "Enclose only accepts a list of tuples"
            for y in x:
                assert isinstance(y, str), "Tuples must be strings"
        
        #Find y (first iteration is opening second is closing)
        openingNotFound = True
        listCounter = 0
        while openingNotFound:
            a = encloseList[listCounter][0] #The tuple element of the list element
            #TODO: I meight need to tokenize this so that it matches with what I will compare it to.
            #Find the next character after the macro have it equal to 'a' 
            macroOpening = self.currentTokenized[self.currentLine][1]#Need to remind myself what the currentTokenized is made up of
            if macroOpening == a:
                openingNotFound = True
            else:
                listCounter += 1
                if listCounter >= len(encloseList):
                    raise Exception("Error: Macro enclosement not found. Rewrite Macro correctly")

        
        
        del self.currentTokenized[self.currentLine]#Remove from the list what was read out
        
        #Find the opening match that is equal to 'a'

    def keep(self, var):
        'Add a variable to the list that say not to hygienize.'
        logging.debug("Keep Method started. List of trackOldVar {}".format(self.trackOldVar))
        for x in aider.getVars(var):
            if x not in self.trackOldVar:
                self.trackOldVar.append(x)
                logging.info("This is keep() variable not added already in list: {}".format(x))
            logging.info("This is keep() variable added: {}".format(x))
        logging.debug("Keep Method finished. New List of trackOldVar {}".format(self.trackOldVar))

    def dirty(self,var):
        'Remove a variable from list of variables. This makes the variable removed hygienized'
        logging.debug("Dirty Method started. List of trackOldVar {}".format(self.trackOldVar))
        for x in aider.getVars(var):
            self.trackOldVar = [var for var in self.trackOldVar if var != x]
            logging.info("This is dirty() variable asked to be removed: {}".format(x))
        logging.debug("Dirty Method finished. New List of trackOldVar {}".format(self.trackOldVar))

    def next(self, *args):
        #self.collect()
        self.blockAdvancer()
        self.hygieneNeeded = 1

        if len(args) == 0:
            'No arguments so variables are not contained or separated'
            logging.debug("Next(). currenttokenized {}".format(self.currentTokenized))
            #tokenizes a string
            #TODO: For this I should use the try except to catch array out of bounds thing and if it is out of bound then I should say macro incorrectly typed
            if self.currentTokenized[self.currentLine][0] == 3:#It is a string
                value = eval(self.currentTokenized[self.currentLine][1]) #This is to make the value of the tokenize string token not be delimited and escaped as a string but viewed as a string intuitively
            else:
                value = self.currentTokenized[self.currentLine][1]#Is the current position of the array where swap was just removed.In this position should be  the next string element like x
            logging.debug("Next method. removing currenttokenized currentline {}".format(self.currentTokenized[self.currentLine]))
            del self.currentTokenized[self.currentLine]#Remove from the list what was read out
            self.trackOldVar.append(value)#NOTE: Tracking the old vars in list
            return value
        else:
            'Otherwise arguments are enclosed'
            value = []
            encloseCount=0
            if args[0] == '(' and len(args) == 1:
                #then I expect to find an enclosing parentheses I take everything in between that and take it out of the array and put it in another array
                #g = list(tokenize.tokenize(BytesIO('()'.encode('utf-8')).readline))[1:-1] #tokenized values of '()' first argument is for ( second is for )
                g = ['(', ')']
                #print('THIS IS CURRENT LOOKED AT {}'.format(self.currentTokenized[self.currentLine]))
                
                pass#TODO: parse and find the ending ')'
            else:
                'If more than one argument other than expected then second argument is enclosing symbol'
                
                """g = list(tokenize.tokenize(BytesIO(args[0].encode('utf-8')).readline))[1:-1]
                g.extend(list(tokenize.tokenize(BytesIO(args[1].encode('utf-8')).readline))[1:-1])"""

                g = [args[0], args[1]]
                
            if self.currentTokenized[self.currentLine][1] ==  g[0]:
                del self.currentTokenized[self.currentLine]
                while self.currentTokenized[self.currentLine][1] != g[1] and encloseCount == 0:
                    print(encloseCount)
                    value.append(self.currentTokenized[self.currentLine][1])
                    del self.currentTokenized[self.currentLine] #remove the element from tokenized array and place in another array
                    if self.currentTokenized[self.currentLine][1] == g[0]:
                        encloseCount += 1
                     
                    elif self.currentTokenized[self.currentLine][1] == g[1]:
                        encloseCount -= 1
                        
                    
                #if (self.currentTokenized[self.currentLine][0], self.currentTokenized[self.currentLine][1]) != (g[1][0], g[1][1]):
                del self.currentTokenized[self.currentLine]  #remove enclosing parentheses
                print('VALUEOFINNEXT{}'.format(value))
                stringvalue = ''
                for ele in value:
                    stringvalue += ele
                print(stringvalue)
                #TODO: self.trackOldVar.append(stringvalue) might be uneeded becasue of custom class of string
                #self.trackOldVar.append(stringvalue)#NOTE:Tracking the old vars in list
                stringvalue = CtxSection(stringvalue, self)
                return stringvalue #value should be all that was enclosed
            else:
                raise Exception("There is error in writing a macro. Check syntax") 

    #TODO:NOTE: START OF ADDITIONAL CONTEXT EATERS
    def nextBlock(self, *args):
        logging.info("nextBlock() method started. Consuming rest of line.")
        lineValue = ''
        self.blockAdvancer()
        holdPrevious = False

        try:
            trackIndents = self.currentTokenized[self.currentLine][2][1]#The 4 in (1, 4)
        except IndexError:
            try:
                self.sDataIndex += 1
                holdPrevious = self.currentLine
                self.currentLine = aider2.macroReplace(self.sData[self.sDataIndex])
                self.currentTokenized = cleanPlaceholders(self.currentTokenized)
                '''while self.currentTokenized[self.currentLine][2] == '' or self.currentTokenized[self.currentLine][2][0] == 0:
                    self.currentLine += 1'''
                trackIndents = self.currentTokenized[self.currentLine][2][1]
            except IndexError:
                self.sDataIndex -=1
                self.currentLine = holdPrevious
                logging.debug("Reached end of file no more information to consume")
                raise Exception("IndexError: end of file pantryContext context 'next' consumption is incorrect.")
        
        for x in range(trackIndents):
            lineValue += ' '
        #TODO:Here I need to make sure it is the next block. I just consume the next line but It does not cut it.
        #blockValue = self.nextLine()
        #lineValue += blockValue
        logging.debug("self.currentLine: {}".format(self.currentLine))
        logging.info("nextBlock() consumed rest of line moving to anticipated block: {}\n{}".format(self.currentTokenized,self.currentTokenized[self.currentLine]))
        '''while self.currentTokenized[self.currentLine][2] == '' or self.currentTokenized[self.currentLine][2][0] == 0:
            self.currentLine += 1'''
        assert self.currentTokenized[self.currentLine][0] != 0, "Block was not found macro error" #The tokenizer module ends sections with this special token
        #5 is the number indicating 'INDENT' in lexer
        columnPosition = self.currentTokenized[self.currentLine][2][1]
        while self.currentTokenized[self.currentLine][2][1] >= columnPosition:
            trackIndents = self.currentTokenized[self.currentLine][2][1]
            for x in range(trackIndents):
                lineValue += ''#TODO: changed to return nothing but orinially a space ' ' 
            if self.currentTokenized[self.currentLine][0] == 0:#If next is endmarkker
                break
            else:
                lineValue += self.nextBlockWhileHelper()
            #if enmarker then break while loop
        #if self.currentTokenized[self.currentLine][0] == 0:#It has reached an endmarker
        #    self.nextBlockWhileHelper() #Consumes endmarker
        logging.debug("nextBlock() finished return value: \n{}".format(lineValue))
        if holdPrevious: self.currentLine = holdPrevious#TODO: Holdprevious might not be needed since I changed the thing in starttoker.
        return lineValue
        '''if 'outer_block' in args:
            while self.currentTokenized[self.currentLine][3][1] == columnPosition: 
                
        else:   
            while self.currentTokenized[self.currentLine][3][1] >= columnPosition:
                self.nextLine()'''

    def nextBlockWhileHelper(self):
        logging.info("nextBlockHelper's while loop looking at: {}".format(self.currentTokenized[self.currentLine]))
        if self.currentTokenized[self.currentLine][0] == 3:#It is a string
                value = eval(self.currentTokenized[self.currentLine][1])
        else:
                value = self.currentTokenized[self.currentLine][1]
        logging.info("nextBlockHelper deleting: {}".format(self.currentTokenized[self.currentLine]))
        del self.currentTokenized[self.currentLine]
        return value

    #stoppingPoint is a tuple cointaining a string value and error message
        #String value determines where in line to stop and a potential
        # Error message is optional. If not there no error msg will be outputted if stoppingPoint is not found. (This is to allow consumption of entire line.) 
    def nextLine(self, stoppingPoint=()):
        'Consumes the entire line unless specified stoppingPoint declared'
        logging.info("nextLine() method started")
        logging.info("nextLine() {}".format(self.currentTokenized))
        logging.info("nextLine() looking at: {}".format(self.currentTokenized[self.currentLine]))
        self.hygieneNeeded = 0
        self.blockAdvancer()
        lineValue = ''
        logging.info("nextLine() looking at: {}".format(self.currentTokenized[self.currentLine]))
        
        #TODO: Add logic for advancing to next file block of nextline is called on the endmarker aka the last thing on sData[sDataindex] array
        #TODO:END Add logic for advancing to next file block

        if self.currentTokenized[self.currentLine][0] == 59: #The encoding token
            self.currentLine += 1

        rowPosition = self.currentTokenized[self.currentLine][2][0] #TODO: CHECK if it continues to increment to the next token
        
        
        while self.currentTokenized[self.currentLine][2][0] == rowPosition:
            logging.info("nextLine's while loop looking at: {}".format(self.currentTokenized[self.currentLine]))
            if self.currentTokenized[self.currentLine][0] == 3:#It is a string
                    value = eval(self.currentTokenized[self.currentLine][1])
            else:
                    value = self.currentTokenized[self.currentLine][1]
            logging.info("nextLine deleting: {}".format(self.currentTokenized[self.currentLine]))
            del self.currentTokenized[self.currentLine]
            lineValue += value
            try:
                logging.debug("nextLine() look at currentLine[2][0]{}".format(self.currentTokenized[self.currentLine][2][0]))
                while self.currentTokenized[self.currentLine][2] == '' or self.currentTokenized[self.currentLine][2][0] == 0:
                    self.currentLine += 1
                self.currentTokenized[self.currentLine][2][0]
            except IndexError:
                logging.debug("nextLine() Reached end of block. Encountered IndexError in nextLine() returns: {}".format(lineValue)) #Tends to be endmarker
                return lineValue

        logging.info("nextLine() next currentline is {}".format(self.currentTokenized[self.currentLine]))
        return lineValue
    #TODO:NOTE: END OF ADDITIONAL CONTEXT EATERS

    #Method to check it it has reached the end of a file block and makes it advance to the next block
    def blockAdvancer(self):
        'If it is at an endmarker then advance to the next file block'
        if self.currentTokenized[self.currentLine][0] == 0: #Tokenizer signal for endMarker
            try:
                self.sDataIndex += 1
                holdPrevious = self.currentLine
                self.currentLine = aider2.macroReplace(self.sData[self.sDataIndex])
                self.currentTokenized = cleanPlaceholders(self.currentTokenized)
            except IndexError:
                self.sDataIndex -= 1
                self.currentLine = holdPrevious
                logging.debug("blockAdvancer: Reached end of file no more information to consume")
                raise Exception("IndexError: end of file 'next' consumption is incorrect.")
        else:
            logging.debug("blockAdvancer()- no endmarker detected did not advance block")





#ctx = Context(preStep.macroPrep)#Creates an object that will be shared with whoever imports this file



'''mtriggers = {}#A dictionary of macro trigger words Everytime a new one is defined it is added to the list with the function attached

def macro(f):
    mtriggers[f.__name__] = f'''



#TODO: Addition of global Context object
def start():
    return Context()
#TODO: END of global Context object


def cleanPlaceholders(result):
    logging.debug("cleanPlaceholders() - cleaning {}".format(result))
    cleanResult = []
    for i, (tType, tName, tStart, tEnd, tLine) in enumerate(result):
        if tType == 1 and tName == '':
            pass#del result[i]
        else:
            cleanResult.append((tType,tName,tStart,tEnd,tLine))
    return cleanResult