import tokenize, re, ast, os, pantryContext
from io import BytesIO

'''
what happens if I have a macro word in a function. This means that it would detect it in a function and comment it out so the ast can recognize it
    If it comments it out it still needs the return available.
    Also if there is a macro in a macro then another problem would occur. 

#This might not be needed: See if I can add the macro pre tag to the start of the word instead of the start of the line

So the problem with having the comment out macros in functions that define macros is that the function when called will have
    the commented out version

There might be a problem with putting moduletest.pretoker.... if they import it as something i.e. from preparser import starttoke as start
    #There does not seem to be any problems though. Need to double check later
'''



class PreParse:
    '''This class contains some housekeeping things to help ensure that the preparsing of the file
    are free of common errors as much as possible.'''
    def __init__(self):
        self.catchCall = 0 #Catches when a macro function is defined
        self.catchBlock = 0
        self.macrPreToke = []
        self.macroPrep = '#@!'
        self.addCompileString = None
        
    def commenter(self, line):
        #print(line)
        regex = r'\(.*\)' #Regex to search for anything within parentheses
        if '@starttoker.macro' in line:#TODO: Think about when it is imported with a different name for this I think we look at the import statement and find out.
            self.catchCall = 1
            return line
        elif self.catchCall == 1:
            if 'def' in line: #Finds a method by looking for 'def'
                look = line.split(' ')
                found = next(x for x in look if 'def' in look)
                ind = look.index(found) + 1
                mObj = re.search(regex, look[ind]) 
                if mObj == None: #Make it so that if it is none it spits out error
                    raise Exception('CusPreParseError: Cannot find function name with Regex.')
                result = look[ind][:mObj.start()]
                self.macrPreToke.append(result)
                self.catchCall = 0
                return line
        elif len(self.macrPreToke) >= 1 and any(x in line for x in self.macrPreToke): #TODO:I don't think I need len(macrpretoke) #Checks if any macro words are in the line 
            self.catchBlock = 1
            leadWhitespace = len(line) - len(line.lstrip())
            return line[:leadWhitespace] + self.macroPrep + line[leadWhitespace:] #TODO: Is there a reason to attach macroPrep to Contex() class? Why not just put it in PreParse()
        elif self.catchBlock == 1: #TODO: THIS is temporary fix to blocks see if I can get something more elegant to see where it belongs in the block
            leadWhitespace = len(line) - len(line.lstrip())
            if 'return' in line: #TODO: I think this if statement is unneeded since the toker will take out all comments at the first step. But it might be needed because the return statement is needed for macros to return string TODO: IS this incorrect? should it be 'return' instead of 'return in line' I don't remember
                return line #NOTE: This is for those methods that if was in a block would have the return avaialable 
            elif leadWhitespace >= 1: #If there is a space in front of the line that means that it is a block
                #leadWhitespace = len(line) - len(line.lstrip())
                return line[:leadWhitespace] + self.macroPrep + line[leadWhitespace:]
            else:
                self.catchBlock = 0
                return line
        else:
            return line

preStep = PreParse()

#TODO: Make sure that this works properly. For example that it is the same context object that is changed that is being used by all the pantry methods.
def ensurePrepUnique(f):
    'This is to make sure that if the symbol combination is used in the file that macroPrep of Context object is unique'
    while preStep.macroPrep in f:
        preStep.macroPrep = preStep.macroPrep + '%'
#TODO: END I did not place it yet. Make sure it's correct in the preToker make sure to test before renabling.


def preToker(s):#TODO: HAVE a additional argument to output files to.
    newFilename='outputFiletest.py'
    newfilepath = None
    with open(s, 'r') as rFile:
        newfilepath = '{}{}'.format(os.path.abspath(rFile.name)[:-(len(os.path.basename(rFile.name)))], newFilename)
        print(newfilepath)
        sfArray = os.path.splitext(newfilepath)
        secondFile = sfArray[0] + '2' + sfArray[1]
        print(secondFile)
        with open(newfilepath, 'w+') as wFile:
            for line in rFile:
                aArray = preStep.commenter(line)
                #print(aArray)
                wFile.write(aArray)
    with open(newfilepath, 'a') as wFile:
        #XXX: CTX object change
        #ctx = pantryContext.Context(preStep.macroPrep)
        ctx = pantryContext.ctx
        ctx.macroPrep = preStep.macroPrep
        #XXX:END CTX object change
        
        #TODO: INSTEAD of adding this string just call it here? Because I need sorted(locals()) to properly hygienize macros.
        preStep.addCompileString="\nstarttoker.startToker('{0}', '{1}', sorted(locals()), {2})".format(newfilepath, secondFile, id(ctx))
        ctx.preparseEndString = preStep.addCompileString
        wFile.write(preStep.addCompileString)
    #TODO: Maybe I don't call the compile here on this file if I just call starttoker.starToker here instead.
    with open(newfilepath) as f:
        print("Running Pantry macros through the file. \n.\n.\n.\n.\n.")
        code = compile(f.read(), 'outputFiletest.py', 'exec')
        exec(code)
        print("Pantry completed!")

#preToker('junkfile.py')