#from preparser import preStep as preStep

class CtxSection(str):
    'For when user request a section of context in next'

    def __init__(self, contents, ctxObj):
        self.contents = contents
        self.ctx = ctxObj

    def split(self, a):
        'When the string is split into an array take what it is split by and put it in context for later retreval'
        self.ctx.trackOldVar = self.contents.split(a)
        return self.ctx.trackOldVar

class Context:
    'after macro find arguments for it'

    #TODO: Make it so that to change these vars you need to use a method. This is for security.
    def __init__(self, macroPrep='#@!'):
        self.itemize = []
        self.currentTokenized = []
        self.currentLine = -1
        self.macroPrep = macroPrep
        self.preparseEndString = None
        #NOTE:Adding old var tracking
        self.hygieneNeeded = 0
        self.trackOldVar = [] #list of old variables
        self.trackNewVar = {}
        self.existingVars = []
        #NOTE: END old var tracking

    '''def collect(self):
        'takes the next part of the string as arguments'
        #For rest of line put it in the array.'''

    #TODO: Finish enclose method
    def enclose(self, encloseList):
        'Takes a list of tuples to find the starting and closing characters that will be part of the macro'
        assert isinstance(encloseList, list), "Enclose takes a list as an argument"
        for x in encloseList:
            assert len(x) == 2, "Tuples must be of length 2"
            assert isinstance(x, tuple), "Enclose only accepts a list of tuples"
            for y in x:
                assert isinstance(y, str), "Tuples must be strings"
        
        #Find y (first iteration is opening second is closing)
        openingNotFound = True
        listCounter = 0
        while openingNotFound:
            a = encloseList[listCounter][0] #The tuple element of the list element
            #TODO: I meight need to tokenize this so that it matches with what I will compare it to.
            #Find the next character after the macro have it equal to 'a' 
            macroOpening = self.currentTokenized[self.currentLine][1]#Need to remind myself what the currentTokenized is made up of
            if macroOpening == a:
                openingNotFound = True
            else:
                listCounter += 1
                if listCounter >= len(encloseList):
                    raise Exception("Error: Macro enclosement not found. Rewrite Macro correctly")

        
        
        del self.currentTokenized[self.currentLine]#Remove from the list what was read out
        
        #Find the opening match that is equal to 'a'


    def next(self, *args):
        #self.collect()
        self.hygieneNeeded = 1

        if len(args) == 0:
            'No arguments so variables are not contained or separated'
            #tonenize a string
            #TODO: For this I should use the try except to catch array out of bounds thing and if it is out of bound then I should say macro incorrectly typed
            if self.currentTokenized[self.currentLine][0] == 3:#It is a string
                value = eval(self.currentTokenized[self.currentLine][1]) #This is to make the value of the tokenize string token not be delimited and escaped as a string but viewed as a string intuitively
            else:
                value = self.currentTokenized[self.currentLine][1]#Is the current position of the array where swap was just removed.In this position should be  the next string element like x
            del self.currentTokenized[self.currentLine]#Remove from the list what was read out
            self.trackOldVar.append(value)#NOTE: Tracking the old vars in list
            return value
        else:
            'Otherwise arguments are enclosed'
            value = []
            encloseCount=0
            if args[0] == '(' and len(args) == 1:
                #then I expect to find an enclosing parentheses I take everything in between that and take it out of the array and put it in another array
                #g = list(tokenize.tokenize(BytesIO('()'.encode('utf-8')).readline))[1:-1] #tokenized values of '()' first argument is for ( second is for )
                g = ['(', ')']
                #print('THIS IS CURRENT LOOKED AT {}'.format(self.currentTokenized[self.currentLine]))
                
                pass#TODO: parse and find the ending ')'
            else:
                'If more than one argument other than expected then second argument is enclosing symbol'
                
                """g = list(tokenize.tokenize(BytesIO(args[0].encode('utf-8')).readline))[1:-1]
                g.extend(list(tokenize.tokenize(BytesIO(args[1].encode('utf-8')).readline))[1:-1])"""

                g = [args[0], args[1]]
                
            if self.currentTokenized[self.currentLine][1] ==  g[0]:
                del self.currentTokenized[self.currentLine]
                while self.currentTokenized[self.currentLine][1] != g[1] and encloseCount == 0:
                    print(encloseCount)
                    value.append(self.currentTokenized[self.currentLine][1])
                    del self.currentTokenized[self.currentLine] #remove the element from tokenized array and place in another array
                    if self.currentTokenized[self.currentLine][1] == g[0]:
                        encloseCount += 1
                     
                    elif self.currentTokenized[self.currentLine][1] == g[1]:
                        encloseCount -= 1
                        
                    
                #if (self.currentTokenized[self.currentLine][0], self.currentTokenized[self.currentLine][1]) != (g[1][0], g[1][1]):
                del self.currentTokenized[self.currentLine]  #remove enclosing parentheses
                print('VALUEOFINNEXT{}'.format(value))
                stringvalue = ''
                for ele in value:
                    stringvalue += ele
                print(stringvalue)
                #TODO: self.trackOldVar.append(stringvalue) might be uneeded becasue of custom class of string
                #self.trackOldVar.append(stringvalue)#NOTE:Tracking the old vars in list
                stringvalue = CtxSection(stringvalue, self)
                return stringvalue #value should be all that was enclosed
            else:
                raise Exception("There is error in writing a macro. Check syntax") 

    #TODO:NOTE: START OF ADDITIONAL CONTEXT EATERS
    def nextBlock(self):
        pass

    def nextLine(self):
        pass
    #TODO:NOTE: END OF ADDITIONAL CONTEXT EATERS

#ctx = Context(preStep.macroPrep)#Creates an object that will be shared with whoever imports this file



'''mtriggers = {}#A dictionary of macro trigger words Everytime a new one is defined it is added to the list with the function attached

def macro(f):
    mtriggers[f.__name__] = f'''



#TODO: Addition of global Context object
ctx = Context()
#TODO: END of global Context object