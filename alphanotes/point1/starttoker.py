import logging, tokenize, re, ast, starttoker, ctypes
from io import BytesIO
#from pantryContext import ctx as ctx
#from pantryContext import mtriggers as mtriggers2#TODO: fix the mtriggers in the macroreplacer function

'''
I need a function that takes a pattern for the macro program to look for.
    Instead of having the function name define the name the name will be explicitly named by the user
    So they would call moduletest.syntax(swap+) and swap+ would be the name of the macro Look at the sweet.js pattern and form they use and try to imitate.
'''

logging.basicConfig(filename='starttoker.log', filemode='w', format='%(name)s - %(levelname)s - %(message)s')

ctx = None #Should be initialized by method StartToker where it will have the correct values

def strToBlock(line):
    'Takes an array that is generated from a file and makes it into an array by blocks'
    nArray = []
    lookForClosingComment = 0
    lookforDefBlock = 0
    for x in line:
        print(repr(x))
        if len(x) - len(x.lstrip()) > 0: #There is a whitespace/indent at string
            try:
                nArray[-1] += x
            except:
                print("ERROR: File has indent on first line")
                raise
        elif "@starttoker.macro" in x:#TODO: Make it so that decorators are added to the same block
            lookforDefBlock = 1
            nArray.append(x)
        elif lookforDefBlock == 1:
            try:
                lookforDefBlock = 0
                nArray[-1] += x
            except:
                print("ERROR: Array out of bounds")
                raise
        else:
            if lookforDefBlock == 1:
                lookforDefBlock = 0
            nArray.append(x)
    
    return nArray


def hygienize(s):
    #NOTE:AST Hygiene implementation
    tree = ast.parse(s)
    for node in ast.walk(tree):
        if isinstance(node, ast.Name):
            if node.id not in ctx.trackOldVar: #Add new vars into set so none is repeated
                if node.id not in ctx.trackNewVar:
                    i = 0
                    hygic = node.id
                    while hygic in ctx.existingVars:
                        if i > 0:
                            hygic = hygic[:hygic.rfind('__')]
                        hygic = hygic + "__{}".format(i)
                        i += 1
                    ctx.trackNewVar[node.id] = hygic
                
                node.id = ctx.trackNewVar[node.id]
                #TODO: I need to make sure the global() gets all the variables in the entire file and not just up til that point where the macro is called
#Then check if new form is in globals. If not then add it if it is change and chech again
#NOTE:AST Hygiene implementation 
#TODO: There is an error where if the ast isn't complete it will give syntax error i.e. while i == 0: will give syntax error

def hygienize2(s):
    #NOTE:REPLACE NEW VARS WITH HYGIENE
    for i,(tType, tName, a, b, c) in enumerate(s):
        if tType == tokenize.NAME and tName in ctx.trackNewVar:
            tName = ctx.trackNewVar[tName]
            s[i] = (tType, tName, a, b, c)
    
    ctx.trackOldVar = []#NOTE: Cleans tracker
    ctx.trackNewVar = {}#NOTE: Cleans tracker
    #NOTE:END REPLACE NEW VARS WITH HYGIENE'''


mtriggers = {}#A dictionary of macro trigger words Everytime a new one is defined it is added to the list with the function attached

'''def macro(aList):
    #There is an error with this but I decided to approach it anothe way anyway.
    def macro_decorate(f):
        for x in aList:
            assert isinstance(x,str), "Macro only accepts a List of Strings"
        multiple = len(aList)
        if multiple >= 0:
            while multiple < 0:
                mtriggers[f.__name__+aList[multiple-1]] = f
                multiple -= 1
        else:
            mtriggers[f.__name__] = f'''
def macro(f):
    mtriggers[f.__name__] = f

#TODO: Change the readline variable
def macroReplace(readline, mtriggers = mtriggers):#TODO: fix the mtriggers2 var in this to make mtriggers name different in funtion body
    
    '''#TODO: Making block into a list to ease processing Dont' do this since it moots the point of making into blocks
    newArray = readline.split('\n')
    #TODO: Making block to list end'''
    
    
    result = []
    logicBitIgnore = 0 #To make it ignore the def of the macro

    ctx.currentTokenized = list(tokenize.tokenize(BytesIO(readline.encode('utf-8')).readline))

    #TODO: CLEAN UP TOKEN LIST
    newList = []
    for (tType, tName, tStart, tEnd, tLine) in ctx.currentTokenized:
        newList.append((tType,tName, tStart, tEnd, ''))#NOTE:OLDVERSION#, tStart, tEnd, ''))
    ctx.currentTokenized = newList
    #TODO:CLEAN UP END

    
    positionColumnAdjust = 0#To adjust the position of tokens
    positionRowAdjust = 0

    for i,token in enumerate(ctx.currentTokenized):
        print("THIS IS TOKEN{}".format(token))
        tType = token[0]#TODO: I can put all this in for loop above instead of listing it out
        tName = token[1]
        tStart = token[2]
        tEnd = token[3]
        tLine = token[4]

        
        ctx.currentLine = i
        #print("THIS IS BITIGNORE {}".format(logicBitIgnore))
        if tType == tokenize.NAME and tName == 'def':#If the tuple has it as 'def' then the next tuple is not a replaceable macro
            logicBitIgnore = 1
            #print("HERE BIT IGNORE IS CHANGED {}".format(logicBitIgnore))
            result.append((tType, tName, tStart, tEnd, tLine))
        elif logicBitIgnore == 1: #Ignores the next word for example 'def swap'. swap will be ignored
            logicBitIgnore = 0
            result.append((tType, tName, tStart, tEnd, tLine))

        #NOTE:WORKAROUND for the Syntax error
        elif tType == tokenize.COMMENT and tName[:len(ctx.macroPrep)] == ctx.macroPrep: #TODO:Added tName in mtriggers and make sure correct
            del ctx.currentTokenized[ctx.currentLine]#Remove the ctx.macroPrep and replace the tokenize array
            tokeString = list(tokenize.tokenize(BytesIO(tName[len(ctx.macroPrep):].encode('utf-8')).readline))
            tokeString = tokeString[1:-1]#So that it does not take encoding and endmarker
            pos = ctx.currentLine
            for token in tokeString:#NOTE: I do this because the start and end doesn't match up so I get rid of them so they pose no problem. If I want these arguments then I need to add addition to them to get the proper values when I add the macro into the array
                ctx.currentTokenized.insert(pos,(token[0],token[1], '', '', ''))#TODO: Make it match the positioning
                pos += 1
            ctx.currentTokenized.insert(ctx.currentLine, (1, '', '', '', ''))#Position adjustment
            
            '''newList = ctx.currentTokenized[:ctx.currentLine]
            newList.extend(mResult)
            newList.extend(ctx.currentTokenized[ctx.currentLine:])
            ctx.currentTokenized = newList'''
            print('THISISAFTERCOMMENT{}'.format(ctx.currentTokenized))
            print('THIS IS POS {}'.format(ctx.currentLine))


            """tokeString = list(tokenize.tokenize(BytesIO(tName[len(ctx.macroPrep):].encode('utf-8')).readline))
            tokeString = tokeString[1:-1]#So that it does not take encoding and endmarker
            print('HERE IS TOKENSTRING{}'.format(tokeString))


            continueMacroReplace = 0 #TODO: I need to take into account multiple macros in the same comment line
            #TODO: I want to make this recursive TODO: TODO: TODO: TODO: TODO:
            for (ttType, ttName, _, _, _) in tokeString:
                if ttName in mtriggers:
                    continueMacroReplace = 1

            if continueMacroReplace == 1:
                del ctx.currentTokenized[ctx.currentLine]#Remove the ctx.macroPrep and replace the tokenize array
                pos = ctx.currentLine
                for token in tokeString:#NOTE: I do this because the start and end doesn't match up so I get rid of them so they pose no problem. If I want these arguments then I need to add addition to them to get the proper values when I add the macro into the array
                    ctx.currentTokenized.insert(pos,token)
                    pos += 1
                ctx.currentTokenized.insert(ctx.currentLine, (1, ''))#Position adjustment

                '''newList = ctx.currentTokenized[:ctx.currentLine]
                newList.extend(mResult)
                newList.extend(ctx.currentTokenized[ctx.currentLine:])
                ctx.currentTokenized = newList'''
                print('THISISAFTERCOMMENT{}'.format(ctx.currentTokenized))
                print('THIS IS POS {}'.format(ctx.currentLine))
            else:
                continueMacroReplace = 0
                result.append((tType,tName))

            #TODO: PROBLEM LIES HERE"""
        #NOTE:WORKAROUND END

        elif tType == tokenize.NAME and tName in mtriggers: #TODO:THIS IS WHERE I NEED TO REOPTIMIZE AND TAKE ANOTHER LOOK AT WHEN I HAVE TIME
            print("Found {}".format(tName))
            #print(tType, tName)

            #NOTE: TEST
            print('THIS IS MTRIGGERS DELETED{}'.format(ctx.currentTokenized[ctx.currentLine+1]))
            del ctx.currentTokenized[ctx.currentLine]
            macroString = mtriggers[tName]()#call the function in the dictionary
            

            #hygienize(macroString)#TODO: problem when the replacement is not a complete line of code ast can't parse it
            #if macroString == None: macroString = '' #TODO: HAVEN'T CHECK QUICK FIX
            
            tokeString = list(tokenize.tokenize(BytesIO(macroString.encode('utf-8')).readline))
            tokeString = tokeString[1:-1]
            

            #hygienize2(tokeString)
            
            
            pos = ctx.currentLine
            for token in tokeString:#NOTE: I do this because the start and end doesn't match up so I get rid of them so they pose no problem. If I want these arguments then I need to add addition to them to get the proper values when I add the macro into the array
                ctx.currentTokenized.insert(pos,(token[0], token[1]))
                pos += 1
            ctx.currentTokenized.insert(ctx.currentLine, (1, '', '','',''))#Position adjustment

            #TODO: Trying to see if I can detokenize whole line do hygiene then tokenize it again
            print('HYGIENE')
            #print(ctx.currentTokenized[ctx.currentLine:])
            retoken = ctx.currentTokenized#[ctx.currentLine:]#TODO: Try the entire line without it being cut
            #retoken[0] = (59, 'utf-8')
            print('retoken')
            print(retoken)
            #print(retoken[ctx.currentLine])
            #print(retoken[ctx.currentLine-1])


            #####TODO: GET RID OF EXTRA TUPLES INFORMATION NAOT NEEDED
            newretoken = []
            for token in retoken:
                ttType = token[0]
                ttName = token[1]
                newretoken.append((ttType, ttName))
            retoken = newretoken
            print(retoken)

            nRetoken=retoken#This block takes out the placeholder so there is no mistake of the extra space which is seen as indent
            recordLocal = []
            for i, (tType, tName) in enumerate(retoken):
                if tType == 1 and tName == '':
                    recordLocal.append(i)
                    del nRetoken[i]
            retoken=nRetoken
            ####TODO:END EXTRA TUPLES

            
            retoken = tokenize.untokenize(retoken).decode('utf-8')#TODO:THIS IS OLD VERSION#retoken = tokenize.untokenize(ctx.currentTokenized).decode('utf-8')
            print('HERE')
            #print(repr(retoken))
            retokeList = list(retoken)
            #print(retokeList)
            x=0
            while x < len(retokeList) and list(retoken)[x] == ' ' :#TODO: and x < len(retokeList) is just a quick fix need to check
                print("WHILELOOOOP")
                x+=1
            print(x)
            print(list(retoken))
            retoken = retoken[x:]#For some reason it adds a space at untokenize here
            print(list(retoken))
            print(repr(retoken))
            '''for x, i in enumerate(retoken):
                if i == '\n':
                    del retoken[x]
                    del retoken[x]
                    retoken.insert(x, ' ')
                    retoken.insert(x, '\n')'''
            '''newRetoken = repr(retoken).replace(' \n ', '  \n')
            newRetoken = retoken.replace('4 \n', '4')'''
            '''if retoken == 'print (x );x +=1 \n while x <4 \n:print (x );x +=1 ':
                newRetoken = newRetoken = 'print (x );x +=1 \nwhile x <4:print (x );x +=1'
                retoken = newRetoken'''
            #newRetoken = 'print (x );x +=1 \nwhile x <4:print (x );x +=1'
            #newRetoken = retoken.replace(' while', 'while')
            '''newRetoken = retoken.replace(' \n ', '  \n')
            newRetoken = retoken.replace('<4 \n:', '4:')
            newRetoken = retoken.replace(' while', 'while')'''

            print('NEWRETOKEN')
            print(retoken)
            if ctx.hygieneNeeded == 1: hygienize(retoken)
            print(ctx.trackNewVar)
            print('HERE')
            retoken = list(tokenize.tokenize(BytesIO(retoken.encode('utf-8')).readline))
            

            #TODO: EXPERIMENT REINSERTING block start
            print('RECORDLOCAL REINSERT {}'.format(recordLocal))
            for i,a in enumerate(recordLocal):#TODO:TESTED REinserting the removed (1,'') placeholders 
                if i == len(recordLocal)-1:#TODO: quick fix figure out why it adds and extra location
                    pass
                else:
                    retoken.insert(a, (1,'','','',''))
            recordLocal = []
            #TODO:EXPERIMENT BLOCK END



            print('HERE')
            if ctx.hygieneNeeded == 1: hygienize2(retoken)
            print(retoken)
            print("RETOKENCURRENT")
            print(retoken[ctx.currentLine])
            for i in range(x):
                retoken.insert(ctx.currentLine-1, (1, '', '', '', ''))
            retoken.insert(ctx.currentLine-1, (1, '', '', '', ''))#TODO: Figure out what is wrong with iteration that without these statements it gets rid of parts of the list
            #retoken.insert(ctx.currentLine-1, (1, '', '', '', ''))
            
            ctx.currentTokenized[:] = retoken #NOTE: The error here is that it will still continue to use the old list even though I changed it here. 
            #So To fix this I need to change the current list being traversed through

            print('HYGIENE')
            print(len(retoken), len(ctx.currentTokenized))
            print('HYGIENE')
            print(ctx.currentTokenized)

            '''mResult = []# I can do list comprehension here to get rid of mResult array
            for tType, tName, _, _, _ in tokeString:#Note I do this because the start and end doesn't match up so I get rid of them so they pose no problem. If I want these arguments then I need to add addition to them to get the proper values when I add the macro into the array
                mResult.append((tType, tName)) 
            newList = ctx.currentTokenized[:ctx.currentLine]
            newList.extend(mResult)
            newList.extend(ctx.currentTokenized[ctx.currentLine:])
            ctx.currentTokenized = newList'''

            '''mResult = []
            for tType, tName, _, _, _ in ctx.currentTokenized:#Note I do this because the start and end doesn't match up so I get rid of them so they pose no problem. If I want these arguments then I need to add addition to them to get the proper values when I add the macro into the array
                mResult.append((tType, tName))
            resultingString = tokenize.untokenize(mResult).decode('utf-8')
            print(resultingString)'''

            #NOTE: TESTEND

        else:
            result.append((tType, tName, '', '', ''))

    print(result)
    block = [indent for indent in result if indent[0] == 5]


    #NOTE:GET RID OF THE PLACEHOLDERS USED FOR ITERATION
    simpResult = []
    for x in result:
        simpResult.append((x[0], x[1]))
    result = simpResult
    print('HERE IS NEW RESULT {}'.format(result))

    for i, (tType, tName) in enumerate(result):
        if tType == 1 and tName == '':
            del result[i]
    #NOTE:RID OF PLACEHOLDERS END


    nresult = tokenize.untokenize(result).decode('utf-8')
    '''for i in range(len(block)):
        nresult = '\t' + nresult'''
    return nresult


        
    #TODO: Search string for "\n#@!" or "\n -whitespaces-#@!" and remove the #@! or whatever the macroPrep string is
    
    #TODO: Find the macro words to replace so swap becomes tmp = x\n x = y\n y=x NOTE: I do not need the repr() of the string returned by the macro function
    #TODO: Note there is a slight problem if the macro word is commonly used or part of another word. For example I name a variable do and I use a variable doLoop = 1. 
        #The variable doLoop will be looked at and the do will be replaced.
        #I need to see if I can replace a dictionary key's name If I can then I can add a method
        #This method would be the contextify method that takes the next section and delimits the macro
        #If this method is not called then I look for just the macro word that means 'swap ' should be looked for
        # But if it is delimited then I should look for 'swap()' and the starting delimiter and ending demlimeter should take anything in between.
        # I should also consider having the .next to have a variable that increments to tell how many variables are expected in that function
        # and have the dictionary be a tuple i.e.(function, #ofvars)


def startToker(s, s2, s3, ctxObject):
    nData = []
    global ctx 
    ctx = ctypes.cast(ctxObject, ctypes.py_object).value #This is to take the memory location from id() and retrieve it as an object
    ctx.existingVars = s3
    with open(s, 'r') as rFile: #TODO: Make the other 'with open' command make it not nested
        with open(s2, 'w+') as wFile:
            sData = rFile.readlines()
            #sData = repr(data)#TODO: Receprocated delete

            sData = strToBlock(sData)#Makes the blocks of the file into one element instead of several
            print(sData)
            
            #XXX: START OF COMPOSABLE
            print('STARTING COMPOSABLE BLOCK')
            affectedIndexes = [] #TODO: I might remove this if I leave it unused. Use this to remove from the macro expansion body step macro definitions
            unfinishedMacros = [] #Macros that have macros in its definition
            completeMacros = {} #Macros that do not need to be expanded. This will contain the macro name as the key and the function reference of that macro
            '''trackDefMacro = 0'''
            for i,element in enumerate(sData):#First search all elements for the macro definitions
                if '@starttoker.macro' in element:#Is it the definition of a macro?#TODO: I need to make it so that it recognized macros if they import it differently
                    affectedIndexes.append(i) #Adds the index number of the block that is a macro
                    '''trackDefMacro = 1
                elif trackDefMacro == 1:#Was the previous element saying it was defining a macro?'''
                    print('THIS IS ELEMENT WITH @STARTTOKER\n{}'.format(element))
                    if ctx.macroPrep in element:#Does the macro definition have macros
                        unfinishedMacros.append(element) #Add the macro to an array to expand it before it can be used to expand
                        print('HERE IS UNFINISHEDMACROS {}'.format(unfinishedMacros))
                    else: #It contains no macros found by preparse
                        macroName = r'def (.*)\(.*\)' #TODO:regex for finding function name
                        print('HERE IS ELEMENT{}'.format(element))
                        match = re.search(macroName, element)
                        print(match.group(1))
                        completeMacros[match.group(1)] = mtriggers[match.group(1)]#Add to dictionary the macro name, function pair with completed macros
                        print('HERE IS COMPLETEMACROS{}'.format(completeMacros))

            #TODO: Put the method back where it checks for if the macro has already been through and if they stay the same it is infinite loop so exit with error code
            while len(unfinishedMacros) > 0: #While there still are unfinished macros
                unfinishedList = unfinishedMacros[:]#TODO: Iterating through a list that you remove elements from will cause problems
                for element in unfinishedList:
                    newDefMacro = macroReplace(element, completeMacros) #macro replace macro words that are completed if the macro isn't in the completed list it should not remove the preparse macro comment
                    if ctx.macroPrep in newDefMacro: 
                        pass
                    else:#if it isn't in there add it to list of completed macros
                        macroName = r'def (.*)\(.*\)'
                        match = re.search(macroName, newDefMacro)
                        print('HERE IS ELEMENT {}'.format(newDefMacro))#TODO: THERE IS ERROR SINCE it compiles it in this file instead of the other file
                        exec(newDefMacro)#Evaluate the function and add that function to the dictionary it should rewrite the function in the mtriggers dictionary
                        completeMacros[match.group(1)[:-1]] = mtriggers[match.group(1)[:-1]]#TODO: the -1 is to get rid of space that tokenizer adds #add the newly changed function to the completeMacros
                        unfinishedMacros.remove(element)
                        iNumber = sData.index(element)
                        del sData[iNumber]
                        sData.insert(iNumber, newDefMacro)
                    

            print(sData)
            '''wData = ''.join(sData)
            wFile.write(wData)'''
            #XXX:END OF COMPOSABLE
            

            print('HERE IS MTRIGGERS{}'.format(mtriggers))
            for element in sData:#Go through array and see if it has a macro
                if ctx.macroPrep in element: #Does it have a macro in it or might have a macro? in other words did preparser preparse it
                    print('MACROPREP IS IN THIS ELEMENT{}'.format(element))
                    element = macroReplace(element) #the file with the macros replaced
                nData.append(element)


            sData = ''.join(nData)
            #print('HERE IS THE SINGLE STRING{}'.format(sData))
            #TODO: make this deleted this added string at the end of the file. Do a reverse (from the end) string search. Then cut off that section.
            #TODO: Also make this not so hard coded and have the string be flexible
            #catchEndCommand = "\nstarttoker.startToker('outputFiletest.py', 'outputFiletest2.py', sorted(locals()))"
            catchEndCommand = ctx.preparseEndString
            replaceResult = sData.partition(catchEndCommand)
            print(replaceResult)
            if replaceResult[1] == '' and replaceResult[2] == '': #Second 'and' statement just to make sure
                raise Exception("Error: End Command not found unexpected")
            else:
                wData = replaceResult[0] + replaceResult[2] #replaceResult[2] is probably uneeded but just in case something appended to end of file


            wFile.write(wData)
    #XXX: CTX OBJECT CHANGE
    return ctx #TODO: Check fidelity since it is outside with opens above
    #XXX: END CTX OBJECT CHANGE


#TODO:NOTE: UNDO COMMENT ABOVE THE """
           
            '''for line in rFile:
                if 'moduletest.startToker(' in line:#TODO: Have it write it in terms of the file. If they import it as something you should call it as that term
                    pass #Don't write this line to final output
                else:
                    aArray = m_decorator(line)
                    print(aArray)
                    wFile.write(aArray)'''




'''def startTokerTest():#NOTE: TESTING without composable macros
    pass
'''









def strtoBlockTest():#NOTE:TESTING strToBlock
    s = """import moduletest, os
from moduletest import ctx as ctx

@moduletest.macro
def swap():
    x = ctx.next()
    y = ctx.next()
    print("SWAP ACTIVATED")
    return "tmp = {0}; {0} = {1}; {1} = tmp".format(x,y)
    
print('hi')
'''swap a b:
    swap a b
    return True
print('test')'''
    """
    x = s.split('\n')
    arr = []
    for y in x:
        y += '\n'
        arr.append(y)
    #arr = ['import moduletest, os\n', 'from moduletest import ctx as ctx\n', '\n', '@moduletest.macro\n', 'def swap():\n', '    x = ctx.next()\n', '    y = ctx.next()\n', '    print("SWAP ACTIVATED")\n', '    return "tmp = {0}; {0} = {1}; {1} = tmp".format(x,y)\n', '\n', "print('hi')\n", "'''swap a b:\n", '    swap a b\n', '    return True\n', "print('test')'''\n", '\n', 'print("END")']
    print(strToBlock(arr))

#strtoBlockTest()