import starttoker, os
from starttoker import ctx as ctx

@starttoker.macro
def test(ctx=ctx):
    condition = ctx.nextLine()
    print("cond ACTIVATED")
    return "{}".format(condition)

@starttoker.macro
def test2(ctx=ctx):
    condition = ctx.nextBlock()
    print('block activated')
    return "{}".format(condition)

#Real Code
x = 0
test(x > 7)
test2:
    while(x==1):
        print('hi')
        x += 1