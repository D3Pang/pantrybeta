import tokenize, logging
from io import BytesIO

def lumberjack(fileName, name, filemode='w', lFormat='%(asctime)s.%(msecs)d: %(name)s - %(levelname)s - %(message)s', level=logging.DEBUG):
    'Quick set up of logging module '
    handler = logging.FileHandler(fileName, mode=filemode)
    handler.setFormatter(logging.Formatter(lFormat,'%m-%d-%y %H:%M:%S'))

    logger = logging.getLogger(name)
    logger.setLevel(level)
    logger.addHandler(handler)

    return logger



def toker(s):
    tokenObject = tokenize.tokenize(BytesIO(s.encode('utf-8')).readline)
    return tokenObject

def getVars(s):
    aList=[]
    for tType, tName, _, _, _ in toker(s):
        if tType == tokenize.NAME:
            aList.append(tName)
    return aList

def blockTest():
    s = """import moduletest, os
from moduletest import ctx as ctx

@moduletest.macro
def swap():
    x = ctx.next()
    y = ctx.next()
    print("SWAP ACTIVATED")
    return "tmp = {0}; {0} = {1}; {1} = tmp".format(x,y)
    
print('hi')
'''swap a b:
    swap a b
    return True
print('test')'''
    """
    return s


"""def macroReplace(x, mtriggers):
    return list(tokenize.tokenize(BytesIO(x.encode('utf-8')).readline))"""